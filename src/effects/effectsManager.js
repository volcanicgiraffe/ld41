
export function createBlood1(x, y) {
    let sprite = game.add.sprite(x, y, 'effects', 0, game.effectsGrp);
    sprite.anchor.set(1, 0.5);
    sprite.animations.add('hit2', Phaser.Animation.generateFrameNames('hit2_', 1, 7, '.png', 1), 12);
    sprite.animations.play('hit2', null, false, true);
    return sprite;
}

export function createBlood2(x, y) {
    let sprite = game.add.sprite(x, y, 'effects', 0, game.effectsGrp);
    sprite.anchor.set(1, 0.5);
    sprite.animations.add('hit3', Phaser.Animation.generateFrameNames('hit3_', 1, 6, '.png', 1), 12);
    sprite.animations.play('hit3', null, false, true);
    return sprite;
}

export function createBloodRand(x, y) {
    if (Math.random() > 0.5) {
        return createBlood1(x, y);
    } else {
        return createBlood2(x, y);
    }
}

export function createMeatChunk(x, y) {
    const direction = { x: x + game.rnd.integerInRange(-25, 25), y: y + game.rnd.integerInRange(-25, 25) }
    let sprite = game.add.sprite(x, y, 'effects', 0, game.effectsGrp);
    sprite.anchor.set(1, 0.5);
    sprite.animations.add('splat', ["splat1_1.png", "splat1_1.png", "splat1_1.png", "splat1_1.png", "splat1_1.png", "splat1_1.png",
        "splat1_2.png", "splat1_3.png", "splat1_4.png"], 24);
    game.add.tween(sprite).to(direction, 300, null, true).onComplete.addOnce(() => sprite.destroy());
    sprite.animations.play('splat');
    return sprite;
}

export function createHeal(x, y) {
    let sprite = game.add.sprite(x, y, 'effects', 0, game.effectsGrp);
    sprite.anchor.set(0.5, 1);
    sprite.animations.add('heal', Phaser.Animation.generateFrameNames('heal', 1, 17, '.png', 1), 12);
    sprite.animations.play('heal', null, false, true);
    return sprite;
}

export function createLoopHeal(x, y) {
    let sprite = game.add.sprite(x, y, 'effects', 0, game.effectsGrp);
    sprite.anchor.set(0.5, 1);
    sprite.animations.add('heal', Phaser.Animation.generateFrameNames('heal_l_', 1, 7, '.png', 1), 12);
    sprite.animations.play('heal', null, true);
    return sprite;
}

export function createMeatChunks(x, y) {
    for (let i = 0; i < 6; i++) {
        createMeatChunk(x, y);
    }
}

export function createBlueExplosion(x, y) {
    let sprite = game.add.sprite(x, y, 'effects', 0, game.effectsGrp);
    sprite.anchor.set(0.5, 1);
    sprite.animations.add('bexp', Phaser.Animation.generateFrameNames('blue_exp', 1, 8, '.png', 1), 12);
    sprite.animations.play('bexp', null, false, true);
    return sprite;
}

export function createFire(x, y) {
    let sprite = game.add.sprite(x, y, 'effects', 0, game.effectsGrp);
    sprite.anchor.set(0.5, 0.5);
    sprite.animations.add('fire', Phaser.Animation.generateFrameNames('fire1_', 1, 6, '.png', 1), 12);
    sprite.animations.play('fire', null, true);
    return sprite;
}

export function createHit(x, y) {
    let sprite = game.add.sprite(x, y, 'effects', 0, game.effectsGrp);
    sprite.anchor.set(0.5, 0.5);
    sprite.animations.add('hit1', Phaser.Animation.generateFrameNames('hit1_', 1, 7, '.png', 1), 12);
    sprite.animations.play('hit1', null, false, true);
    return sprite;
}

export function createPowerupStar(x, y) {
    let sprite = game.add.sprite(x, y, 'effects', 0, game.effectsGrp);
    sprite.anchor.set(0.5, 1);
    sprite.animations.add('star', Phaser.Animation.generateFrameNames('powerup_star', 1, 5, '.png', 1), 12);
    sprite.animations.play('star', null, false, true);
    return sprite;
}

export function createLavaFlick(x, y) {
    let sprite = game.add.sprite(x, y, 'effects', 0, game.effectsGrp);
    sprite.anchor.set(1, 0.5);
    sprite.animations.add('flick', Phaser.Animation.generateFrameNames('lava_flick_', 1, 9, '.png', 1), 12);
    sprite.animations.play('flick', null, false, true);
    return sprite;
}

export function createSlimeEffect(x, y) {
    let sprite = game.add.sprite(x, y, 'effects', 0, game.effectsGrp);
    sprite.anchor.set(0.5, 1);
    sprite.frameName = 'slime_effect.png';
    return sprite;
}

export function createConfetti1(x, y) {
    let sprite = game.add.sprite(x, y, 'effects', 0, game.effectsGrp);
    sprite.anchor.set(0.5, 1);
    sprite.scale.x = Math.random() < 0.5 ? -1 : 1;
    sprite.animations.add('conf', Phaser.Animation.generateFrameNames('conf1_', 1, 9, '.png', 1), 12);
    sprite.animations.play('conf', null, false, true);
    return sprite;
}

export function createConfetti2(x, y) {
    let sprite = game.add.sprite(x, y, 'effects', 0, game.effectsGrp);
    sprite.anchor.set(0.5, 1);
    sprite.scale.x = Math.random() < 0.5 ? -1 : 1;
    sprite.tint = Math.random() * 0xffffff;
    sprite.animations.add('conf', Phaser.Animation.generateFrameNames('conf2_', 1, 7, '.png', 1), 12);
    sprite.animations.play('conf', null, false, true);
    return sprite;
}

export function createConfetti3(x, y) {
    let sprite = game.add.sprite(x, y, 'effects', 0, game.effectsGrp);
    sprite.anchor.set(0.5, 1);
    sprite.animations.add('conf', Phaser.Animation.generateFrameNames('conf3_', 1, 7, '.png', 1), 12);
    sprite.animations.play('conf', null, false, true);
    return sprite;
}

export function createConfettiRand(x, y) {
    if (Math.random() > 0.60) {
        return createConfetti1(x, y);
    } else if (Math.random() > 0.20) {
        return createConfetti2(x, y);
    } else  {
        return createConfetti3(x, y);
    }
}