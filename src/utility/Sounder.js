export default class Sounder {

  constructor() {
    this.s = [];

    this.add('kick', ['kick_1', 'kick_2', 'kick_3', 'kick_4'], 300);
    this.add('kick_hard');

    this.add('swoosh', ['swoosh_1', 'swoosh_2', 'swoosh_3'], 300);

    this.add('fireball');
    this.add('flap');
    this.add('bonus');
    this.add('crust');
    this.add('heal');
    this.add('catch');
    this.add('guard');

    this.add('step', ['step_1'], 150);
    this.add('step_d', ['step_d_1'], 150);
    this.add('step_h', ['step_h_1'], 200);

    this.add('whistle');
    this.add('goal');

    this.add('ui_click');
    this.add('ui_click_2');
  }

  destroy() {
    this.s.forEach(s => s.destroy());
  }

  add(group, sounds = [group], cooldown = 500) {
    this[group] = {
      sounds: sounds.map(name => this.addSound(name, group)),
      cooldown: cooldown,
      canPlay: true
    }
  }

  addSound(name, group) {
    let s = game.add.sound(name);
    s.onPlay.add(() => {
      this[group].canPlay = false;
      game.time.events.add(this[group].cooldown, () => {
        this[group].canPlay = true;
      });
    });
    this.s.push(s);
    return s;
  }

  play(group) {
    if (this[group].canPlay) {
      game.rnd.pick(this[group].sounds).play();
    }
  }
}
