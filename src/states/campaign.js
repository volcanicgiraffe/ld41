import Kickor from "objects/units/kickor";
import Biggy from "objects/units/biggy";
import Wizzard from "objects/units/wizzard";
import Bes from "objects/units/bes";
import Obstacle from "objects/obstacle";
import Beholder from "objects/units/beholder";
import Slime from "objects/units/slime";
import Threclops from 'objects/units/threclops';
import Devil from 'objects/units/devil';
import Knight from 'objects/units/knight';
import { ADDRCONFIG } from "dns";

const Levels = [
    { //LVL1
        p1: {
            slots: [
                { row: 9, col: 3 },
                { row: 12, col: 4 },
                { row: 6, col: 3 },
            ],
            team: [Kickor, Biggy, Kickor]
        },

        p2: {
            slots: [
                { row: 9, col: 3 },
                { row: 12, col: 4 },
                { row: 6, col: 3 },
            ],
            team: [Bes, Bes, Bes]
        },

        balls: [
            { row: 9, col: 12 }
        ]
    },
    { // LVL2
        p1: {
            slots: [
                { row: 9, col: 2 },
                { row: 5, col: 8 },
                { row: 9, col: 6 },
                { row: 13, col: 8 },
                { row: 9, col: 10 },
            ],
            team: [Biggy, Kickor, Knight, Kickor, Biggy]
        },

        p2: {
            slots: [
                { row: 9, col: 22 },
                { row: 5, col: 16 },
                { row: 9, col: 18 },
                { row: 13, col: 16 },
                { row: 9, col: 14 }
            ],
            team: [Beholder, Bes, Bes, Bes, Beholder]
        },

        balls: [
            { row: 9, col: 12 }
        ]
    },
    { // LVL3
        p1: {
            slots: [
                { row: 9, col: 2 },
                { row: 5, col: 8 },
                { row: 9, col: 6 },
                { row: 13, col: 8 },
                { row: 9, col: 10 },
            ],
            team: [Knight, Kickor, Wizzard, Kickor, Knight]
        },

        p2: {
            slots: [
                { row: 9, col: 22 },
                { row: 5, col: 16 },
                { row: 9, col: 18 },
                { row: 13, col: 16 },
                { row: 9, col: 14 }
            ],
            team: [Slime, Bes, Threclops, Bes, Slime]
        },

        balls: [
            { row: 9, col: 12 }
        ]
    },
    { // LVL4
        p1: {
            slots: [
                { row: 9, col: 2 },
                { row: 5, col: 8 },
                { row: 9, col: 6 },
                { row: 13, col: 8 },
                { row: 9, col: 10 },
            ],
            team: [Biggy, Kickor, Wizzard, Kickor, Knight]
        },

        p2: {
            slots: [
                { row: 9, col: 22 },
                { row: 5, col: 16 },
                { row: 9, col: 18 },
                { row: 13, col: 16 },
                { row: 9, col: 14 }
            ],
            team: [Threclops, Bes, Devil, Bes, Beholder]
        },

        balls: [
            { row: 9, col: 12 }
        ]
    }
]

export default class Campaign {
    getNextLevelIndex() {
        return localStorage.getItem('nextLevel') || 0;
    }
    
    winLevel() {
        localStorage.setItem('nextLevel', this.getNextLevelIndex + 1);
    }

    getConfig() {
        return Levels(this.getNextLevelIndex());
    }

}

