import { makeButton } from "../ui/Button";

export const P1_VS_AI = 'P1vsAI';
export const P1_VS_P2 = 'P1vsP2';
export const AI_VS_AI = 'AIvsAI';

export default {
  create() {
    game.stage.backgroundColor = "#a7a57f";

    this.bg = game.add.sprite(0, game.canvas.height, 'title_bg');
    this.bg.anchor.set(0, 1);
    this.bgLogo = game.add.sprite(576, 36, 'title_logo');
    this.bgLogo.y = -this.bgLogo.height;

    this.bgLogoT = game.add.sprite(330, 390, 'text_logo');
    this.bgLogoT.y = -this.bgLogoT.height;
    this.pupil = game.add.sprite(384, 406, 'title_pupil');
    this.pupil.alpha = 0;
    this.pupil.anchor.set(0.5);
    this.startPoint = new Phaser.Point(960, 368);
    this.gloss = game.add.sprite(320, 306, 'title_gloss');
    this.gloss.alpha = 0.8;
    this.bgLogo.addChild(this.pupil);
    this.bgLogo.addChild(this.gloss);

    this.bg.inputEnabled = true;

    this.bg.width = game.canvas.width;
    this.bg.scale.y = this.bg.scale.x;

    this._theme = game.menuMusic = game.add.sound('soccer2');
    this._theme.play(undefined, undefined, 1, true);

    game.add.tween(this.bgLogo).to({ y: 36 }, 900, Phaser.Easing.Elastic.Out, true, 300);
    const t1 = game.add.tween(this.bgLogoT).to({ y: 390 }, 900, Phaser.Easing.Elastic.Out, true, 300);
    const tP = game.add.tween(this.pupil).to({ y: 346, alpha: 1 }, 200, Phaser.Easing.Linear.Out);
    t1.chain(tP);
    tP.onComplete.addOnce(() => {
      this.watch = true;
    })

    this._initMenu();

    if (typeof ga === "function") ga('send', 'event', 'state', 'menu');
  },

  _initMenu() {
    let dx = game.canvas.width * 0.5;
    let dy = game.canvas.height * 0.65;

    makeButton(dx, dy + 80 * 2, 'Start Game', () => game.state.start("select"));
  },

  _startGame(mode) {
    game.winScore = 1; // todo configurable
    game.mode = mode;
    game.add.tween(game.world).to({ alpha: 0 }, 100, null, true).onComplete.add(() => {
      game.state.start("level1");
    });
  },

  shutdown() {
    //this._theme.destroy();
  },

  update() {
    if (this.watch && game.input.activePointer.x > 0 && game.input.activePointer.y > 0) {
      const p = new Phaser.Point(game.input.activePointer.x - this.startPoint.x, game.input.activePointer.y - this.startPoint.y);
      const d = Math.min(Phaser.Point.distance(game.input.activePointer, this.startPoint)*0.2, 60);
      p.normalize().multiply(d, d);
      this.pupil.x = p.x + 384;
      this.pupil.y = p.y + 346;
    }
  }
};
