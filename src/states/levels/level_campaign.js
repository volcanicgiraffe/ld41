import LevelBase from './levelbase';
import Ball from "../../objects/ball";
import Gates from "../../objects/gates";
import Obstacle from "../../objects/obstacle";
import { SELECTION_RED, SELECTION_GREEN } from 'effects/selection';
import Player from "../../model/player";
import PlayerAI from "../../model/player_ai";
import PlayerAISmart from "../../model/player_ai_smart";

export default class LevelCampaign extends LevelBase {

  initLevel() {
    //game.rnd.sow([1])
    this.logic.addGates(new Gates(this.p1, false), {row: 9, col: 0});
    this.logic.addGates(new Gates(this.p2, true), {row: 9, col: 24});

    this._fillObstacles();
    this._fillBalls();
    this._fillUnits();
    this.logic.updateMovementOrder();

    if (typeof ga === "function") ga('send', 'event', 'level', 'LevelCampaign', this._statsName());
  }

  initPlayers() {
    this.p1 = new Player("Player 1", 0, SELECTION_GREEN);
    this.p2 = new Player("Player 2", 1, SELECTION_RED);

    this.p1.opponent = this.p2;
    this.p2.opponent = this.p1;

    if (!game.mode.playerHuman[0]) this.p1.ai = new PlayerAISmart();
    if (!game.mode.playerHuman[1]) this.p2.ai = new PlayerAISmart();

    this.logic.addPlayer(this.p1);
    this.logic.addPlayer(this.p2);
  }

  // TODO: all positions must be % 2 === 0, don't ask why =)
  _fillObstacles() {
    this.logic.addObstacle(new Obstacle(10, 12));
    this.logic.addObstacle(new Obstacle(12, 4));
    this.logic.addObstacle(new Obstacle(2, 2));
    this.logic.addObstacle(new Obstacle(20, 14));
  }

  _fillUnits() {
    this.p1.slots = game.mode.p1.slots  || CONFIG.p1.slots;
    this.p2.slots = game.mode.p2.slots  || CONFIG.p2.slots;

    for (let i = 0; i < Math.max(this.p1.slots.length, this.p2.slots.length); i++) {
      if (this.p2.slots[i]) {
        let unit = new (game.mode.p2.team[i])(this.p2);
        unit.slot = this.p2.slots[i];
        this.logic.addUnit(unit, this.p2.slots[i]).locate().seeAt(this.logic.balls[0]);
      }
      if (this.p1.slots[i]) {
        let unit = new (game.mode.p1.team[i])(this.p1);
        unit.slot = this.p1.slots[i];
        this.logic.addUnit(unit, this.p1.slots[i]).locate().seeAt(this.logic.balls[0]);
      }
    }
  }

  _fillBalls() {
    for (let i = 0; i < CONFIG.balls.length; i++) {
      let ball = new Ball();
      ball.onUnitHit = () => this.onUnitHit();
      ball.slot = CONFIG.balls[i];
      this.logic.addBall(ball, CONFIG.balls[i]).locate();
    }
  }

  get surpriseDropCooldown() {
    return 4;
  }

  get surpriseDropChance() {
    return 0.3;
  }

}
