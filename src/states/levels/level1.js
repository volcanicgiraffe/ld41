import LevelBase from './levelbase';
import Kickor from "../../objects/units/kickor";
import Biggy from "../../objects/units/biggy";
import Wizzard from "../../objects/units/wizzard";
import Bes from "../../objects/units/bes";
import Ball from "../../objects/ball";
import Gates from "../../objects/gates";
import Obstacle from "../../objects/obstacle";
import Beholder from "../../objects/units/beholder";
import Slime from "../../objects/units/slime";
import Threclops from 'objects/units/threclops';
import Devil from 'objects/units/devil';
import Knight from 'objects/units/knight';

// Put gates start positions here if needed, as well as drop rates, allowed abilities, obstacles and etc.
const CONFIG = {
  p1: {
    slots: [
      {row: 9, col: 2},
      {row: 5, col: 8},
      {row: 9, col: 6},
      {row: 13, col: 8},
      {row: 9, col: 10},
    ],
    team: [Wizzard, Kickor, Knight, Kickor, Biggy]
  },

  p2: {
    slots: [
      {row: 9, col: 22},
      {row: 5, col: 16},
      {row: 9, col: 18},
      {row: 13, col: 16},
      {row: 9, col: 14}
    ],
    team: [Slime, Bes, Devil, Bes, Threclops]
  },

  balls: [
    {row: 9, col: 12}
  ]
};

export default class Level1 extends LevelBase {

  initLevel() {
    //game.rnd.sow([1])
    this.logic.addGates(new Gates(this.p1, false), {row: 9, col: 0});
    this.logic.addGates(new Gates(this.p2, true), {row: 9, col: 24});

    this._fillObstacles();
    this._fillBalls();
    this._fillUnits();
    this.logic.updateMovementOrder();

    if (typeof ga === "function") ga('send', 'event', 'level', 'Level1', this._statsName());
  }

  // TODO: all positions must be % 2 === 0, don't ask why =)
  _fillObstacles() {
    this.logic.addObstacle(new Obstacle(10, 12));
    this.logic.addObstacle(new Obstacle(12, 4));
    this.logic.addObstacle(new Obstacle(2, 2));
    this.logic.addObstacle(new Obstacle(20, 14));
  }

  _fillUnits() {
    this.p1.slots = CONFIG.p1.slots;
    this.p2.slots = CONFIG.p2.slots;

    for (let i = 0; i < Math.max(this.p1.slots.length, this.p2.slots.length); i++) {
      if (this.p2.slots[i]) {
        let unit = new (CONFIG.p2.team[i])(this.p2);
        unit.slot = this.p2.slots[i];
        this.logic.addUnit(unit, this.p2.slots[i]).locate().seeAt(this.logic.balls[0]);
      }
      if (this.p1.slots[i]) {
        let unit = new (CONFIG.p1.team[i])(this.p1);
        unit.slot = this.p1.slots[i];
        this.logic.addUnit(unit, this.p1.slots[i]).locate().seeAt(this.logic.balls[0]);
      }
    }
  }

  _fillBalls() {
    for (let i = 0; i < CONFIG.balls.length; i++) {
      let ball = new Ball();
      ball.onUnitHit = () => this.onUnitHit();
      ball.slot = CONFIG.balls[i];
      this.logic.addBall(ball, CONFIG.balls[i]).locate();
    }
  }

  get surpriseDropCooldown() {
    return 4;
  }

  get surpriseDropChance() {
    return 0.3;
  }

}
