import HexGrid from "../../model/grid";
import HexGridView from "../../model/gridview";
import Logic from "../../model/logic";
import StatePanel from "../../ui/StatePanel";
import * as ef from 'effects/effectsManager'
import FanManager from 'objects/fanManager';
import SurpriseBox from '../../objects/surprise_box'
import Player from "../../model/player";
import { SELECTION_RED, SELECTION_YELLOW } from "../../effects/selection";
import PlayerAI from "../../model/player_ai";
import { AI_VS_AI, P1_VS_AI, P1_VS_P2 } from "../menu";
import WinPanel from "../../ui/WinPanel";
import PlayerAISmart from "../../model/player_ai_smart";

const OFFSET_X = 200;
const OFFSET_Y = 84;

const BG_OBSTACLES = [
  // top-left
  { col: 0, row: 0 }, { col: 0, row: 1 }, { col: 0, row: 2 }, { col: 0, row: 3 }, { col: 1, row: 0 }, { col: 2, row: 0 },
  { col: 1, row: 1 }, { col: 0, row: 4 }, { col: 0, row: 5 },
  // bottom-left
  { col: 0, row: 17 }, { col: 0, row: 16 }, { col: 0, row: 15 }, { col: 1, row: 17 },
  { col: 1, row: 16 }, { col: 1, row: 15 }, { col: 2, row: 17 },
  { col: 0, row: 14 }, { col: 0, row: 13 },
  // top-right
  { col: 23, row: 0 }, { col: 24, row: 1 }, { col: 23, row: 2 }, { col: 24, row: 3 }, { col: 23, row: 4 }, { col: 24, row: 5 },
  { col: 21, row: 0 }, { col: 22, row: 0 }, { col: 23, row: 1 },
  // bottom-right
  { col: 24, row: 17 }, { col: 23, row: 17 }, { col: 22, row: 17 }, { col: 23, row: 16 }, { col: 22, row: 16 },
  { col: 24, row: 15 }, { col: 23, row: 15 }, { col: 23, row: 14 }, { col: 24, row: 13 },
];

export default class LevelBase {

  create() {
    if (game.menuMusic) {
      game.menuMusic.destroy();
    }
    game.world.alpha = 1;
    this.bgGroup = game.bgGroup = game.add.group();
    this.bg = game.add.sprite(0, 0, "bg", null, this.bgGroup);
    this.field = game.add.sprite(0, 0, "field", null, this.bgGroup);
    this.gridGrp = game.add.group();
    this.fansGrp = game.add.group();
    this.mainGrp = game.add.group();
    this.effectsGrp = game.add.group();
    game.effectsGrp = this.effectsGrp;
    this.fans = new FanManager(this.fansGrp);

    game.ui = game.add.group(game.world, 'UI');
    game.ui.fixedToCamera = true;

    this.setOffset(this.gridGrp, this.mainGrp, this.effectsGrp);
    game.grid = this.grid = new HexGrid(25, 18);
    this.fillBgObstacles();
    this.fillAutoRicochetCells();
    game.gridview = this.gridview = new HexGridView(this.grid, this.gridGrp);

    this.hexUnderMouse = null;
    this.chosenUnit = null;
    this.chosenBall = null;

    this.inAnimation = false;
    this.pendingPromises = [];

    game.addTurnAnimation = this.addTurnAnimation.bind(this);

    this.logic = game.logic = new Logic(this.grid, this.gridview, this.mainGrp);

    this._surpriseDropCooldown = this.surpriseDropCooldown;

    this.initPlayers();
    this.initLevel();
    this.grid.initPathfinding();
    this.statePanel = new StatePanel(0, 0);

    this.logic.onSwitch.add(this.onUnitSwitch.bind(this));
    this.logic.onActionDone.add(this.onActionDone.bind(this));
    this.logic.onAbilityChanged.add(this.onAbilityChanged.bind(this));

    this.logic.start();

    this.music = game.add.sound('soccer1');
    this.music.play(undefined, undefined, 1, true);

    this.goalSign = game.add.sprite(678, -50, 'ui', 'goal.png');
    this.goalSign.alpha = 0;
    this.winP1Sign = game.add.sprite(666,-50, 'ui', 'rp_win.png');
    this.winP1Sign.alpha = 0;
    this.winP2Sign = game.add.sprite(666, -50, 'ui', 'lp_win.png');
    this.winP2Sign.alpha = 0;
  }

  initPlayers() {
    this.p1 = new Player("Player 1", 0, SELECTION_YELLOW);
    this.p2 = new Player("Player 2", 1, SELECTION_RED);

    this.p1.opponent = this.p2;
    this.p2.opponent = this.p1;

    if (game.mode === AI_VS_AI) this.p1.ai = new PlayerAISmart();
    if (game.mode !== P1_VS_P2) this.p2.ai = new PlayerAISmart();

    this.logic.addPlayer(this.p1);
    this.logic.addPlayer(this.p2);
  }

  fillAutoRicochetCells() {
    this.grid.hexes.forEach(hex => {
      if ((hex.neighbours.length < 6 || hex.neighbours.some(hex => hex.flags.outside)) && !hex.flags.goal) {
        hex.flags.autoricochet = true;
      }
    })
  }

  addTurnAnimation(promise) {
    if (typeof promise.promise === "function") promise = promise.promise();
    this.pendingPromises.push(promise);
  }

  onUnitHit() {
    this.fans.onSmash();
  }

  onAbilityChanged() {
    this.updateMode();
  }

  checkDrop() {
    this._surpriseDropCooldown--;
    if (this._surpriseDropCooldown <= 0) {
      if (game.rnd.frac() < this.surpriseDropChance) {
        this.dropSurprise();
      }
      this._surpriseDropCooldown = this.surpriseDropCooldown;
    }
  }


  onUnitSwitch() {
    if (this.chosenUnit && this.chosenUnit.alive) this.chosenUnit.setSelected(false);
    this.chosenUnit = this.logic.currentUnit;
    this.chosenUnit.setSelected(true);
    this.updateMode();
  }

  dropSurprise(pos = undefined) {
    let surpriseBox = new SurpriseBox();
    this.logic.addSurpriseBox(surpriseBox, pos);
    surpriseBox.locate();
  }

  onActionDone(action) {
    this.onBallSelect(null);
    this._lastAction = action;
    switch (action.type) {
      case "skip":
        if (action.overwatch) {
          //todo: overwatch animation
        }
        this.doTurnAnimation(() => new Promise((resolve) => resolve()));
        break;
      case "move":
        this.doTurnAnimation(() => {
          action.unit.walk();
          return action.unit.moveByAction(action)
            .then(() => action.unit.stop());
        });
        break;
      case "free_move":
        this.doTurnAnimation(() => {
          // TODO: speed up?
          action.unit.walk();
          return action.unit.followPath(this.grid.findPath(action.from, action.to))
            .then(() => {
              action.unit.stop();
              action.unit.movingToSlot = false;

              this._checkAfterGoal();
            });
        });
        break;
      case "kick":
        this.doTurnAnimation(() => {
          return action.ball.flyByKick(action);
        });
        break;
      default:
        throw new Error("unknown action type!");
    }
    if (action.unit) action.unit.animatePreparedAction();

  }

  onAnimationDone() {
    //todo: check is our turn
    this.inAnimation = false;
    game.inAnimation = false;
    this.gridGrp.visible = true;

    this.updateMode();
    this.hexUnderMouse = null;
  }

  updateMode() {
    this.grid.forEach(hex => {
      hex.flags.canWalk = false;
      hex.flags.canPass = false;
      hex.flags.canKick = false;
      hex.flags.canKickBallHere = false;
    });

    switch (this.mode) {
      case "walking": {
        this.logic.getWalkablePoints(this.chosenUnit).forEach(hex => {
          hex.flags.canWalk = true;
          hex.flags.canKickBallHere = (this.logic.canKick(this.chosenUnit, { hex }));
        });
        this.logic.balls.forEach(ball => ball.setSelected(false));
        this.logic.getBallsToInteract(this.chosenUnit).forEach(ball => ball.setSelected(true, true));
        break;
      }
      case "kicking": {
        if (this.chosenUnit.canKick) {
          game.logic.getKickablePoints(this.chosenUnit, this.chosenBall).forEach(hex => {
            hex.flags.canKick = true;
            if (this.logic.findUnitToGetPass(hex, this.chosenUnit)) {
              hex.flags.canPass = true;
            }
          });
        }
      }

    }

  }

  onBallSelect(ball) {
    this.logic.balls.forEach(ball => ball.setSelected(false));
    this.chosenBall = ball;
    if (this.chosenBall) {
      this.chosenBall.setSelected(true);
    }
    this.updateMode();
  }

  get mode() {
    if (this._movingToSlots) return "none";
    if (this.chosenUnit && this.chosenBall) return "kicking";
    if (this.chosenUnit) return "walking";
    return "none";
  }

  fillBgObstacles() {
    BG_OBSTACLES.forEach((hexlike) => {
      let cell = this.grid.getHexBy(hexlike.col, hexlike.row);
      if (!cell || !cell.isFreeToStand) return;
      cell.flags.obstacle = true;
      cell.flags.outside = true;
    });
  }

  initLevel() {

  }

  setOffset(...groups) {
    for (let grp of groups) {
      grp.x = OFFSET_X;
      grp.y = OFFSET_Y;
    }
  }

  getMousePointerInGrid() {
    return {
      x: game.input.mousePointer.worldX - OFFSET_X,
      y: game.input.mousePointer.worldY - OFFSET_Y
    }
  }

  waitForPendingAnimations() {
    if (this.pendingPromises.length) {
      let promises = this.pendingPromises.slice();
      console.log('wait for ', promises.length, 'anims to stop')
      this.pendingPromises = [];
      clearTimeout(this._guard);
      return new Promise((resolve) => {
        let isResolved = false;
        let doResolve = () => {
          clearTimeout(this._guard);
          if (!isResolved) {
            isResolved = true;
            resolve();
          }
        };
        this._guard = setTimeout(() => {
          console.warn("guard after 10s");
          doResolve();
        }, 10 * 1000);
        Promise.all(promises).then(() => this.waitForPendingAnimations()).then(doResolve);
      });
    } else {
      return new Promise((resolve) => resolve());
    }
  }

  doTurnAnimation(fn) {
    this.inAnimation = true;
    game.inAnimation = true;
    this.gridGrp.visible = false;
    this.pendingPromises = [];
    this._lastPromise = fn();
    this._lastPromise
      .then(() => this.checkDrop())
      .then(() => this.logic.beforeEndTurn())
      .then(() => this.waitForPendingAnimations())
      .then(() => this.afterAllAnimations());
  }

  afterAllAnimations() {
    this.logic.afterTurn();
    this.onAnimationDone();
    this.logic.checkEndTurn(this.chosenUnit);
  }

  _sortUnits() {
    this.mainGrp.sort('sortY')
  }

  _checkMassacre() {
    for (let b of game.logic.balls) {
      let brect = b.rect;
      for (let fan of this.fans.fans1) {
        if (!fan.alive) continue;
        let frect = fan.frect || (fan.frect = new Phaser.Rectangle(fan.left, fan.top, fan.width, fan.height));
        if (Phaser.Circle.intersectsRectangle(brect, frect)) {
          fan.dieBy(b);
        }
      }
    }
  }

  update() {
    this._sortUnits();

    if (this._matchCompeted) return;

    this._checkWinCondition();
    this._checkGoal();
    this._checkMassacre();

    if (this.inAnimation || this._movingToSlots) return;

    this._handleChosenUnit();
  }


  render() {
    //game.debug.text("Turn player: " + game.logic.currentPlayer.name + ", unit: " + game.logic.currentUnit.constructor.name, 4, 10);

    //game.debug.text("Mouse: " + this._isDown + ", real " + game.input.mousePointer.isDown, 4, 10);
    //game.debug.text('FPS: ' + (game.time.fps || '--'), 4, game.canvas.height - 48);
    //this.fans.fans1.forEach(fan => game.debug.geom(fan.frect), "green");
    //game.logic.balls.forEach(ball => game.debug.geom(ball.collider.getBounds(), "blue"));
    //game.logic.forEachUnit(unit => game.debug.geom(unit.collider.getBounds(), "green"));
    //game.debug.text('Cell: ' + (this.hexUnderMouse ? (this.hexUnderMouse.row + ',' + this.hexUnderMouse.col) : 'none'), 4, game.canvas.height - 24);
    //game.debug.text('Mouse: ' + (this.getMousePointerInGrid().x + "," + this.getMousePointerInGrid().y), 104, game.canvas.height - 24);

    // this.renderAi();
  }

  shutdown() {
    this.music.destroy();
    game.inAnimation = false;
    game.movingToSlots = false;
  }

  renderAi() {
    this.grid.forEach(hex => {
      if (hex._aiKickWeight) {
        let p = this.grid.getMiddlePoint(hex);
        game.debug.text(hex._aiKickWeight, OFFSET_X + p.x - 10, OFFSET_Y + p.y - 6);
      }

      if (hex._aiMoveWeigth) {
        let p = this.grid.getMiddlePoint(hex);
        game.debug.text(hex._aiMoveWeigth, OFFSET_X + p.x - 10, OFFSET_Y + p.y + 6);
      }
    })
  }

  _handleChosenUnit() {
    if (!this.chosenUnit) return;

    let player = this.logic.getPlayer(this.chosenUnit.playerId);

    if (player.isAi) {
      this._handleAiInput(player);
    }

    let { x, y } = this.getMousePointerInGrid();
    let hexUnderMouse = this.gridview.getHexBy(x, y);

    if (!hexUnderMouse) return;

    if (hexUnderMouse !== this.hexUnderMouse) {
      this.hexUnderMouse = hexUnderMouse;
      this.grid.forEach(hex => hex.flags.highlighted = false);
    }
    switch (this.mode) {
      case "kicking":
        if (hexUnderMouse.flags.canKick) {
          hexUnderMouse.flags.highlighted = true;
          if (!hexUnderMouse.flags.canPass) {
            this.grid.getInRadius(hexUnderMouse, this.logic.getMissRadius(this.chosenUnit, this.chosenBall)).forEach(hex => {
              if (hex.flags.canKick) hex.flags.highlighted = true;
            })
          }
        }
        break;
      case "walking":
        if (hexUnderMouse.flags.canWalk) {
          hexUnderMouse.flags.highlighted = true;
          let path = this.grid.findPath(this.chosenUnit.hex, hexUnderMouse);
          for (let hex of path) {
            hex.flags.highlighted = true;
          }
        }
        break;
    }

    if (!player.isAi) {
      this._handleUserInput(hexUnderMouse);
    }
  }

  _checkWinCondition() {
    if (this._movingToSlots) return; // just to show win screen a bit after the goal

    if (this.p1.score >= game.winScore || this.p2.score >= game.winScore) {
      game.sounder.play('whistle');
      this._matchCompeted = true;
      this.win();
    }
  }

  win() {
    this.winPanel = new WinPanel();
    this.winPanel.fillInfo(this);

    if (typeof ga === "function") ga('send', 'event', 'win', this._statsName(), `${this.p1.score} : ${this.p2.score}`);
  }

  _checkGoal() {
    if (this._movingToSlots) return;
    for (let ball of this.logic.balls) {
      let hexUnderBall = this.gridview.getHexBy(ball.x, ball.y);

      if (hexUnderBall && hexUnderBall.flags.goal) {
        this.fans.onGoal(hexUnderBall.goal.opponent);
        hexUnderBall.goal.opponent.score += 1;
        game.sounder.play('goal');

        if (typeof ga === "function") ga('send', 'event', 'goal', this._statsName(), `${hexUnderBall.goal.opponent.name} - ${hexUnderBall.goal.opponent.isAi ? "AI" : "Human"}`);
        game.add.tween(this.goalSign).to({ y: 80, alpha: 1 }, 400, Phaser.Easing.Quadratic.InOut, true)
          .onComplete.addOnce(() => {
            game.add.tween(this.goalSign).to({ y: 140, alpha: 0 }, 200, Phaser.Easing.Quadratic.InOut, true, 300)
              .onComplete.addOnce(() => {
                this.goalSign.y = -50;
              });
          });
        game.camera.shake(0.01, 300, Phaser.Camera.SHAKE_HORIZONTAL);
        game.camera.flash();

        this._movingToSlots = game.movingToSlots = true;
        game.tweens.removeFrom(ball);
        ball.flyTween = null;
        this.onAnimationDone();

        this.logic.relocateUnit(ball, ball.slot);
        ball.locate();

        this.logic.players.forEach((player) => {
          player.units.forEach((unit) => {
            unit.movingToSlot = true;
            unit.reset();
            this.logic.moveToSlot(unit);
          });
        });
      }
    }
  }

  _handleAiInput(player) {
    if (!this.chosenUnit) return;

    let ai = player.ai;
    ai.performBestAction(this.chosenUnit);
  }

  _handleUserInput(hexUnderMouse) {
    if (!this.chosenUnit) return;

    if (game.input.mousePointer.isDown) {
      if (!this._isDown) {
        this._isDown = true;
        // effects test
        // this.ind = this.ind || 0;
        // let funcs = Object.getOwnPropertyNames(ef).filter(p => typeof ef[p] === 'function');
        // ef[funcs[this.ind]](game.input.mousePointer.worldX - OFFSET_X, game.input.mousePointer.worldY - OFFSET_Y)
        // this.ind++;
        // if (this.ind > funcs.length - 1) this.ind = 0;
        if (hexUnderMouse === this.chosenUnit.hex) {
          this.onBallSelect(null);
        } else if (hexUnderMouse.ball && this.logic.canKick(this.chosenUnit, hexUnderMouse.ball)) {
          this.onBallSelect(hexUnderMouse.ball);
        } else if (this.chosenUnit.canWalk && hexUnderMouse.flags.canWalk) {  //todo[grishin]: not sure it is best way to check we are in walk stage
          this.logic.moveUnitTo(this.chosenUnit, hexUnderMouse);
        } else if (this.chosenUnit.canKick && this.chosenBall && hexUnderMouse.flags.canKick) {
          this.logic.kickBallTo(this.chosenUnit, this.chosenBall, hexUnderMouse);
        }
      }
    } else {
      this._isDown = false;
    }
  }

  _checkAfterGoal() {
    let stillMovingToSlot = false;

    this.logic.players.forEach((player) => {
      player.units.forEach((unit) => {
        if (unit.movingToSlot) stillMovingToSlot = true;
      });
    });

    // if (this._movingToSlots !== stillMovingToSlot) game.sounder.play('whistle');
    this._movingToSlots = game.movingToSlots = stillMovingToSlot;
  }

  _statsName() {
    return `${this.p1.isAi ? 'AI1': 'P1'} - ${this.p2.isAi ? 'AI2': 'P2'}`
  }
}
