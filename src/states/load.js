import Sounder from "../utility/Sounder";

export default {
  loadingLabel() {
    let loading = game.add.sprite(game.world.centerX, game.world.centerY + 100, "loading");
    loading.anchor.setTo(0.5, 0.5);

    let bar = game.add.sprite(game.world.centerX, game.world.centerY - 45, "loading_giraffe");
    bar.anchor.setTo(0.5, 1);

    let barBg = game.add.sprite(game.world.centerX, game.world.centerY, "loading_island");
    barBg.anchor.setTo(0.5, 0.5);

    game.load.setPreloadSprite(bar, 1);
  },

  preload() {
    this.loadingLabel();

    this._loadMusic();
    this._loadSounds();
    this._loadPictures();

  },

  create() {
    game.sounder = new Sounder();

    game.state.start("menu");
  },

  _loadPictures() {
    game.load.image("title_bg", "assets/title/title_bg1.png");
    game.load.image("text_logo", "assets/title/text_logo.png");
    game.load.image("title_logo", "assets/title/title_logo.png");
    game.load.image("title_pupil", "assets/title/title_pupil.png");
    game.load.image("title_gloss", "assets/title/title_gloss.png");

    game.load.image("logo_red", "assets/title/logo_red.png");
    game.load.image("title_bg_red", "assets/title/title_bg_red.png");
    game.load.image("versus_logo", "assets/title/versus.png");
    game.load.image("help", "assets/help.png");

    game.load.image("base_book", "assets/title/base_book.png");

    game.load.image("bg", "assets/bg.png");
    game.load.image("field", "assets/field.png");
    game.load.atlas("actors", "assets/actors.png", "assets/actors.json");
    game.load.atlas("effects", "assets/effects.png", "assets/effects.json");
    game.load.atlas("ui", "assets/ui.png", "assets/ui.json");
    game.load.spritesheet('ui_button', "assets/ui_button.png", 225, 72);
    game.load.atlas("effects", "assets/effects.png", "assets/effects.json");
    game.load.atlas("field_elements", "assets/field_elements.png", "assets/field_elements.json");
  },

  _loadMusic() {
    game.load.audio('soccer1', 'assets/music/Alexey Grishin - soccer1.ogg');
    game.load.audio('soccer2', 'assets/music/Alexey Grishin - soccer2.ogg');
  },

  _loadSounds() {
    game.load.audio('whistle', 'assets/sounds/whistle.wav');
    game.load.audio('goal', 'assets/sounds/goal.wav');
    game.load.audio('bonus', 'assets/sounds/bonus.wav');
    game.load.audio('flap', 'assets/sounds/flap.wav');
    game.load.audio('fireball', 'assets/sounds/fireball.wav');

    game.load.audio('kick_1', 'assets/sounds/kick_1.wav');
    game.load.audio('kick_2', 'assets/sounds/kick_2.wav');
    game.load.audio('kick_3', 'assets/sounds/kick_3.wav');
    game.load.audio('kick_4', 'assets/sounds/kick_4.wav');

    game.load.audio('kick_hard', 'assets/sounds/kick_hard.wav');
    game.load.audio('crust', 'assets/sounds/crust.wav');
    game.load.audio('heal', 'assets/sounds/heal.wav');
    game.load.audio('guard', 'assets/sounds/guard.wav');
    game.load.audio('catch', 'assets/sounds/catch.wav');

    game.load.audio('swoosh_1', 'assets/sounds/swoosh_1.wav');
    game.load.audio('swoosh_2', 'assets/sounds/swoosh_2.wav');
    game.load.audio('swoosh_3', 'assets/sounds/swoosh_3.wav');

    game.load.audio('step_1', 'assets/sounds/step_1.wav');
    game.load.audio('step_h_1', 'assets/sounds/step_h_1.wav');
    game.load.audio('step_d_1', 'assets/sounds/step_d_1.wav');

    game.load.audio('ui_click', 'assets/sounds/ui_click.wav');
    game.load.audio('ui_click_2', 'assets/sounds/ui_click_2.wav');

  }
};
