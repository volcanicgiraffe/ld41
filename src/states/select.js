import { makePortraitButton, makeBigPortraitButton } from "../ui/Button";

import Kickor from "objects/units/kickor";
import Biggy from "objects/units/biggy";
import Wizzard from "objects/units/wizzard";
import Bes from "objects/units/bes";
import Obstacle from "objects/obstacle";
import Beholder from "objects/units/beholder";
import Slime from "objects/units/slime";
import Threclops from 'objects/units/threclops';
import Devil from 'objects/units/devil';
import Knight from 'objects/units/knight';

const humanNames = ['Human', 'Human', 'Human', 'Human', 'Human', 'Meatbag', 'LD bro', 'Homo sapiens'];
const aiNames = ['AI', 'AI', 'AI', 'AI', 'AI', 'AI Overlord', 'Metal doom', 'Your CPU', 'Binary oppressor', 'Hundreds of IF conditions'];

const bigs = [Biggy, Wizzard, Beholder, Slime, Threclops, Knight, Devil];
const smalls = [Kickor, Bes];
const centers = [Wizzard, Threclops]
const B_OFFSET = 40;


export default {
  create() {
    this.bg = game.add.sprite(0, 0, 'base_book');
    this.playerHuman = [true, false];
    this.spritesL = [];
    this.spritesR = [];
    this.teamL = [];
    this.teamR = [];
    this.bg.inputEnabled = true;

    this._initMenu();
  },

  switchPlayer(n) {
    game.sounder.play('swoosh');
    this.playerHuman[n] = !this.playerHuman[n];
    this.redrawButtons();
  },

  redrawButtons() {
    this.spriteP1.frameName = this.playerHuman[0] ? 'human_prt.png' : 'ai_prt.png';
    this.spriteP2.frameName = this.playerHuman[1] ? 'human_prt.png' : 'ai_prt.png';
    this.p1Text.text = this.playerHuman[0] ? game.rnd.pick(humanNames) : game.rnd.pick(aiNames);
    this.p2Text.text = this.playerHuman[1] ? game.rnd.pick(humanNames) : game.rnd.pick(aiNames);
  },

  createUnits() {
    Phaser.ArrayUtils.shuffle(bigs);
    this.bigUnits = [];
    bigs.forEach(k => {
      const u = new k({ id: 0 });
      u.visible = false;
      this.bigUnits.push(u);

    });
    this.smallUnits = [];
    smalls.forEach(k => {
      const u = new k({ id: 0 });
      u.visible = false;
      this.smallUnits.push(u);
    });
    this.centerUnits = [];
    centers.forEach(k => {
      const u = new k({ id: 0 });
      u.visible = false;
      this.centerUnits.push(u);
    });
  },

  createBB(x, y, team, sprites) {
    const ind = this.bbi++;//game.rnd.integerInRange(0, bigs.length - 1);
    let s = game.add.sprite(x + 2, y - B_OFFSET, 'actors', this.bigUnits[ind].portraitFrameName);
    s.anchor.set(0.5, 1);
    sprites.push(s);
    team.push(bigs[ind]);
  },

  switchBB(at, team, sprites) {
    game.sounder.play('swoosh');
    const newI = (bigs.indexOf(team[at]) + 1) % bigs.length;
    team[at] = bigs[newI];
    sprites[at].frameName = this.bigUnits[newI].portraitFrameName;
  },

  createSB(x, y, team, sprites) {
    let s = game.add.sprite(x + 2, y - 6, 'actors', this.smallUnits[team === this.teamL ? 0 : 1].portraitFrameName);
    s.anchor.set(0.5, 1);
    sprites.push(s);
    team.push(smalls[team === this.teamL ? 0 : 1]);
  },

  switchSB(at, team, sprites) {
    game.sounder.play('swoosh');
    const newI = (smalls.indexOf(team[at]) + 1) % smalls.length;
    team[at] = smalls[newI];
    sprites[at].frameName = this.smallUnits[newI].portraitFrameName;
  },

  createCB(x, y, team, sprites) {
    let s = game.add.sprite(x + 2, y - 6, 'actors', this.centerUnits[team === this.teamL ? 0 : 1].portraitFrameName);
    s.anchor.set(0.5, 1);
    sprites.push(s);
    team.push(centers[team === this.teamL ? 0 : 1]);
  },

  switchCB(at, team, sprites) {
    game.sounder.play('swoosh');
    const newI = (centers.indexOf(team[at]) + 1) % centers.length;
    team[at] = centers[newI];
    sprites[at].frameName = this.centerUnits[newI].portraitFrameName;
  },

  _initMenu() {
    this.createUnits();
    makePortraitButton(600, 314, 0xf0e088, () => this.switchPlayer(0))
    makePortraitButton(1260, 314, 0x874c42, () => this.switchPlayer(1))
    this.spriteP1 = game.add.sprite(600, 314, 'actors', 'human_prt.png');
    this.spriteP1.anchor.set(0.5, 1);

    game.add.text(680, 230, 'Player 1', { font: `32px Arial`, fill: "black" });
    this.p1Text = game.add.text(680, 275, 'Human', { font: `16px Arial`, fill: "white" })
    const ct = game.add.text(1160, 230, 'Player 2', { font: `32px Arial`, fill: "black" });
    ct.anchor.set(1, 0);
    this.p2Text = game.add.text(1160, 275, 'AI', { font: `16px Arial`, fill: "white" })
    this.p2Text.anchor.set(1, 0);

    this.spriteP2 = game.add.sprite(1260, 314, 'actors', 'ai_prt.png');
    this.spriteP2.anchor.set(0.5, 1);

    this.bbi = 0;


    makeBigPortraitButton(440, 540, 0xffffff, () => { this.switchBB(0, this.teamL, this.spritesL); });
    this.createBB(440, 540, this.teamL, this.spritesL);
    makeBigPortraitButton(440, 840, 0xffffff, () => { this.switchBB(1, this.teamL, this.spritesL); });
    this.createBB(440, 840, this.teamL, this.spritesL);

    makePortraitButton(740, 500, 0xffffff, () => { this.switchSB(2, this.teamL, this.spritesL); });
    this.createSB(740, 500, this.teamL, this.spritesL);
    makePortraitButton(740, 812, 0xffffff, () => { this.switchSB(3, this.teamL, this.spritesL); });
    this.createSB(740, 812, this.teamL, this.spritesL);
    makePortraitButton(650, 650, 0xffffff, () => { this.switchCB(4, this.teamL, this.spritesL); });
    this.createCB(650, 650, this.teamL, this.spritesL);

    makeBigPortraitButton(1380, 540, 0xffffff, () => { this.switchBB(0, this.teamR, this.spritesR); });
    this.createBB(1380, 540, this.teamR, this.spritesR);
    makeBigPortraitButton(1380, 840, 0xffffff, () => { this.switchBB(1, this.teamR, this.spritesR); });
    this.createBB(1380, 840, this.teamR, this.spritesR);

    makePortraitButton(1100, 500, 0xffffff, () => { this.switchSB(2, this.teamR, this.spritesR); });
    this.createSB(1100, 500, this.teamR, this.spritesR);
    makePortraitButton(1100, 812, 0xffffff, () => { this.switchSB(3, this.teamR, this.spritesR); });
    this.createSB(1100, 812, this.teamR, this.spritesR);
    makePortraitButton(1200, 650, 0xffffff, () => { this.switchCB(4, this.teamR, this.spritesR); });
    this.createCB(1200, 650, this.teamR, this.spritesR);

    let buttonE = game.add.button(40, 26, 'ui', () => game.state.start('menu'), this, 'button_back.png', 'button_back.png', 'button_back.png');
    game.add.sprite(680, 0, 'ui', 'button_start.png');
    let buttonS = game.add.button(680, 10, 'ui', () => this._startGame(), this, 'text_glow.png', 'text_base.png', 'text_glow.png');
  },

  _startGame() {
    game.winScore = 3;
    game.mode = {
      type: 'skirmish',
      playerHuman: this.playerHuman,
      p1: { team: this.teamL },
      p2: { team: this.teamR },
    };
    game.add.tween(game.world).to({ alpha: 0 }, 100, null, true).onComplete.add(() => {
      game.state.start("versus");
    });
  },

  shutdown() {
  },

  update() {

  }
};
