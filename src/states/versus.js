

export default {
  create() {
    this.bg = game.add.sprite(0, 0, 'title_bg_red');
    game.world.alpha = 1;
    game.mode.p1.team.forEach((k, i) => {
      const u = new k({ id: 1 });
      u.visible = false;
      const s = game.add.sprite(-100, 130, 'ui', 'player_portrait_gr.png');
      const s1 = game.add.sprite(s.width * 0.5, s.height, 'actors', u.portraitFrameName);
      s1.anchor.set(0.5, 1);
      s.addChild(s1);
      s.alpha = 0.5;
      const t1 = game.add.tween(s).to({ x: 260 + i * 160, alpha: 1 }, 800, Phaser.Easing.Quadratic.InOut, true, (game.mode.p1.team.length - i) * 250);
      t1.onComplete.addOnce(() => {
        game.add.tween(s).to({ x: game.canvas.width + 100, alpha: 1 }, 400, Phaser.Easing.Quadratic.InOut, true, 1500);
      });
    });

    game.mode.p2.team.forEach((k, i) => {
      const u = new k({ id: 1 });
      u.visible = false;
      const s = game.add.sprite(game.canvas.width + 100, 866, 'ui', 'player_portrait_gr.png');
      const s1 = game.add.sprite(s.width * 0.5, s.height, 'actors', u.portraitFrameName);
      s1.anchor.set(0.5, 1);
      s.addChild(s1);
      s.alpha = 0.5;
      const t1 = game.add.tween(s).to({ x: game.canvas.width - 260 - i * 160, alpha: 1 }, 800, Phaser.Easing.Quadratic.InOut, true, (game.mode.p2.team.length - i) * 250);
      t1.onComplete.addOnce(() => {
        game.add.tween(s).to({ x: -100, alpha: 1 }, 400, Phaser.Easing.Quadratic.InOut, true, 1500);
      });
    });

    const vs = game.add.sprite(956, 554, 'versus_logo');
    vs.alpha = 0;
    vs.anchor.set(0.5);
    vs.scale.set(1.1);
    game.add.tween(vs).to({ alpha: 1 }, 300, Phaser.Easing.Quadratic.In, true, 1400);
    game.add.tween(vs.scale).to({ x: 1, y: 1 }, 300, Phaser.Easing.Quadratic.In, true, 1400);
    this.tm = () => this._startGame();
    game.time.events.add(4000, this.tm);
    this.bg.inputEnabled = true;
    this.bg.events.onInputDown.add(() => this._startGame());
  },

  _startGame() {
    game.time.events.remove(this.tm);
    game.add.tween(game.world).to({ alpha: 0 }, 100, null, true).onComplete.add(() => {
      game.state.start(game.mode.type);
    });
  },

  shutdown() {
  },

  update() {

  }
};
