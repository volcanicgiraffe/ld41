import 'pixi';
import 'p2';
import 'phaser';

import init from 'states/init'
import load from 'states/load'
import menu from 'states/menu'
import play from 'states/play'
import select from 'states/select'
import versus from 'states/versus'
import win from 'states/win'
import Level1 from "./states/levels/level1";
import LevelSkirmish from "./states/levels/level_skirmish";


window.game = new Phaser.Game(1920, 1080, Phaser.AUTO, 'canvas', null);
//game.preserveDrawingBuffer = true;
game.antialias = false;

function wrapLevel(kls) {
  let inst;
  return {
    create() {
      game.level = inst = new kls();
      return inst.create();
    },

    update() { return inst.update()},
    render() { return inst.render()},
    shutdown() {
      inst.shutdown();
      inst = null;
    }
  }
}

game.state.add('init', init);
game.state.add('load', load);
game.state.add('menu', menu);
game.state.add('play', play);
game.state.add('select', select);
game.state.add('skirmish', wrapLevel(LevelSkirmish));
game.state.add('versus', versus);
game.state.add('level1', wrapLevel(Level1));
game.state.add('win', win);

game.state.start('init');

//import dat from "dat.gui"
//game.debuggui = new dat.GUI();



Phaser.Signal.prototype.promise = function (sprite = undefined) {
  return new Promise((resolve) => {
    this.addOnce(() => resolve());
    if (sprite instanceof Phaser.Sprite) {
      sprite.events.onKilled.addOnce(() => resolve());
    }
  });
};

Phaser.Tween.prototype.promise = function () {
  return this.onComplete.promise(this.target);
};

