export const SPRINT = {
  type: "sprint", dropChance: 0, sprite: ["ui", "ability_sprint.png"], cost: 2, action: "move",
  description: "Allows unit to run far and kick a ball in one turn",
  useHint: "Click the button and then click on the ball to run'n'hit.",
  caption: "Sprint"
}
export const X2 = {
  type: "x2", dropChance: 1, sprite: ["ui", "ability_strong.png"], cost: 1, action: "kick",
  description: "Increases kick distance",
  useHint: "While near the ball, click the button and then kick the ball.",
  caption: "Power x2",
  requireBall: true
};
export const FIRE = {
  type: "fire", dropChance: 0.8, sprite: ["effects", "fire1_1.png"], cost: 1, action: "kick",
  description: "Lights the ball on fire. Burning ball is very fun.",
  useHint: "While near the ball, use ability and then kick the ball",
  caption: "Fire",
  requireBall: true
};
export const HEAL = {
  type: "heal", dropChance: 0.3, sprite: ["ui", "ability_heal.png"], cost: 2, action: "kick",
  description: "Ball will heal the unit it hits next.",
  useHint: "While near the ball, use ability and then kick the ball",
  caption: "Heal",
  requireBall: true
};
export const K360 = {
  type: "360", dropChance: 0.3, sprite: ["ui", "ability_360shot.png"], cost: 1, action: "kick",
  description: "Allows to kick the ball in any direction",
  useHint: "While near the ball, use ability and then kick the ball",
  caption: "Kick around",
  requireBall: true
};
export const SLIME = {
  type: "slime", dropChance: 0, sprite: ["ui", "ability_slime.png"], cost: 1, action: "kick",
  description: "Makes the ball slippy. It moves farther, but less accurate",
  useHint: "While near the ball, use ability and then kick the ball",
  caption: "Slippy",
  requireBall: true
};
export const DISTANT_KICK = {
  type: "distant", dropChance: 0, sprite: ["ui", "ability_longshot.png"], cost: 1, action: "kick",
  caption: "Longshot",
  useHint: "Use ability to enhance your reach on this turn",
  description: "You can kick the ball while standing 1 hex away"
};
export const EAGLE_EYE = {
  type: "sniper", dropChance: 0.5, sprite: ["ui", "ability_snipershot.png"], cost: 1, action: "kick",
  caption: "Sniper",
  description: "Improves hit accuracy",
  requireBall: true
};

export const SKIP_PSEUDO_ABILITY = {
  type: "skip", dropChance: 0, sprite: ["ui", "ability_skip.png"], tillNextTurn: true,
  caption: "Skip",
  description: "Skip a turn for this unit"
}
export const OVERWATCH_PSEUDO_ABILITY = {
  type: "overwatch", sprite: ["ui", "ability_watch.png"], tillNextTurn: true,
  caption: "Overwatch",
  description: "Skip a turn for this unit. He will kick back the incoming ball."
}

export const INSTANT_HEAL_PSEUDO_ABILITY = {
  type: "instant_heal", dropChance: 0.8
}

export const ABILITIES = [
  SPRINT, X2, FIRE, HEAL, EAGLE_EYE, K360, INSTANT_HEAL_PSEUDO_ABILITY
];

export function dropAbility() {
  let r = game.rnd.frac();
  return game.rnd.pick(ABILITIES.filter(ab => r < ab.dropChance));
}
