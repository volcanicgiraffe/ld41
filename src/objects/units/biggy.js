import BaseUnit, { ATTACK_ANIM_LENGTH } from "./baseunit";
import {DISTANT_KICK} from "../abilities";

export default class BIGGY extends BaseUnit {
  constructor(player) {
    super("actors", "biggy_i.png", player);
    this.base.x = 4;
    this.base.y = 18;
    this.base.animations.add('idle', ['biggy_i.png'], 18, true);
    this.walkAnim = this.base.animations.add('walk', ['biggy_w1.png', 'biggy_w2.png', 'biggy_w3.png', 'biggy_w4.png', 'biggy_w5.png'], 18, true);
    this.walkAnim.onLoop.add(() => {game.sounder.play('step_h')}, this);

    this.base.animations.add('attack', ['biggy_a1.png'].concat(new Array(ATTACK_ANIM_LENGTH)).fill('biggy_a2.png', 1).concat('biggy_i.png'), 18);
    this.base.animations.add('die', ['biggy_d.png'], 12);

    this.addCooldownAbility(DISTANT_KICK, 1);
  }

  get armor() { return 1.2; }

  get baseWalkDistance() { return 3; }
  get baseKickDistance() { return 8; }


  get orderPriority() { return 2; }
}
