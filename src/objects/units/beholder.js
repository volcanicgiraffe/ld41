import BaseUnit, { ATTACK_ANIM_LENGTH } from "./baseunit";
import {EAGLE_EYE} from "../../objects/abilities"

export default class Beholder extends BaseUnit {
  constructor(player) {
    super("actors", "beholder_i.png", player);
    this.base.x = 0;
    this.base.animations.add('idle', ['beholder_i.png'], 18, true);
    this.walkAnim = this.base.animations.add('walk', ['beholder_w1.png', 'beholder_w2.png', 'beholder_w3.png', 'beholder_w4.png', 'beholder_w5.png'], 9, true);

    this.base.animations.add('attack', ['beholder_a1.png'].concat(new Array(ATTACK_ANIM_LENGTH)).fill('beholder_a2.png', 1).concat('beholder_i.png'), 18);
    this.base.animations.add('die', ['beholder_d.png'], 12);

    this.addCooldownAbility(EAGLE_EYE, 1);

  }

  get baseWalkDistance() { return 4; }

  get baseKickDistance() { return 8; }

  get orderPriority() { return 1; }
}
