import BaseUnit, { ATTACK_ANIM_LENGTH } from "./baseunit";
import {SPRINT} from "../../objects/abilities"

export default class Kickor extends BaseUnit {
  constructor(player) {
    super("actors", "sworcher_i.png", player);
    this.base.anchor.x = 0.4;
    this.base.y = 5;
    this.base.animations.add('idle', ['sworcher_i.png'], 18, true);
    this.walkAnim = this.base.animations.add('walk', ['sworcher_w1.png', 'sworcher_w2.png', 'sworcher_w3.png', 'sworcher_w4.png', 'sworcher_w5.png'], 18, true);
    this.walkAnim.onLoop.add(() => {game.sounder.play('step')}, this);

    this.base.animations.add('attack', ['sworcher_a1.png'].concat(new Array(ATTACK_ANIM_LENGTH)).fill('sworcher_a2.png', 1).concat('sworcher_i.png'), 18);
    this.base.animations.add('die', ['sworcher_d.png'], 12);

    this.addCooldownAbility(SPRINT, 1);
  }

  get baseCellMoveTime() { return 220; }

  get orderPriority() { return 3; }
}
