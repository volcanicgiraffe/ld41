import BaseUnit, { ATTACK_ANIM_LENGTH } from "./baseunit";
import {SLIME} from "../abilities";

export default class Slime extends BaseUnit {
  constructor(player) {
    super("actors", "slime_i.png", player);
    this.base.anchor.x = 0.45;
    this.base.anchor.y = 0.8;
    this.base.y = -6;
    this.base.animations.add('idle', ['slime_i.png'], 18, true);
    this.walkAnim = this.base.animations.add('walk', ['slime_w1.png', 'slime_i.png', 'slime_w2.png', 'slime_i.png'], 12, true);
    this.base.animations.add('attack', ['slime_a1.png'].concat(new Array(ATTACK_ANIM_LENGTH)).fill('slime_a2.png', 1).concat('slime_i.png'), 12);
    this.base.animations.add('die', ['slime_d.png'], 12);

    this.addCooldownAbility(SLIME, 2);
  }

  get armor() { return 1.5; }

  get baseWalkDistance() { return 3; }
  get baseKickDistance() { return 8; }


  get orderPriority() { return 1; }
}
