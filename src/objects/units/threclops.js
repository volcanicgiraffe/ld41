import BaseUnit, { ATTACK_ANIM_LENGTH } from "./baseunit";
import { K360 } from "../abilities"

export default class Threclops extends BaseUnit {
  constructor(player) {
    super("actors", "threclops_i.png", player);
    this.base.anchor.x = 0.45;
    this.base.anchor.y = 0.8;
    this.base.y = -6;
    this.base.animations.add('idle', ['threclops_i.png'], 18, true);
    this.walkAnim = this.base.animations.add('walk', ['threclops_w1.png', 'threclops_w2.png', 'threclops_w3.png', 'threclops_w4.png'], 16, true);
    this.walkAnim.onLoop.add(() => {game.sounder.play('step_d')}, this);

    this.base.animations.add('attack', ['threclops_a1.png'].concat(new Array(ATTACK_ANIM_LENGTH)).fill('threclops_a2.png', 1).concat('threclops_i.png'), 12);
    this.base.animations.add('die', ['threclops_d.png'], 12);

    this.addCooldownAbility(K360, 3);
  }

  get armor() { return 1.2; }

  get baseWalkDistance() { return 4; }
  get baseKickDistance() { return 6; }


  get orderPriority() { return 5; }

  get baseActionPoints() { return 3; }
}
