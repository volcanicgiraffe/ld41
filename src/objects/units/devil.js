import BaseUnit from "./baseunit";
import { FIRE } from "../../objects/abilities"

export default class Devil extends BaseUnit {
  constructor(player) {
    super("actors", "bigDevil_i.png", player);
    this.base.anchor.x = 0.5;
    this.base.anchor.y = 0.8;
    this.base.y = -16;
    this.base.animations.add('idle', ['bigDevil_i.png'], 18, true);
    this.walkAnim = this.base.animations.add('walk', ['bigDevil_w1.png', 'bigDevil_w2.png', 'bigDevil_w3.png', 'bigDevil_w4.png', 'bigDevil_w5.png'], 18, true);
    this.walkAnim.onLoop.add(() => {game.sounder.play('step_h')}, this);

    this.base.animations.add('attack', ['bigDevil_a1.png', 'bigDevil_a2.png',
      'bigDevil_a3.png', 'bigDevil_a2.png', 'bigDevil_a3.png',
      'bigDevil_a2.png', 'bigDevil_a3.png', 'bigDevil_i.png'], 18);
    this.base.animations.add('die', ['bigDevil_d.png'], 12);

    this.addCooldownAbility(FIRE, 1);
  }

  get baseWalkDistance() { return 4; }
  get baseKickDistance() { return 6; }

  get orderPriority() { return 2; }

  get baseActionPoints() { return 2; }

  get fireResist() { return true; }

  get armor() { return 1.5; }

  addEffect(effect, updateCooldown = undefined) {
    if (effect.type === "fire") return;
    super.addEffect(effect, updateCooldown);
  }
}
