import GridSprite from "../gridsprite";
import SelectionShader, { SELECTION_YELLOW } from "../../effects/selection";
import HpBar from '../hp_bar';
import { createBloodRand, createHeal } from 'effects/effectsManager';

let ID = 1;
export const ATTACK_ANIM_LENGTH = 6;
export const SPRINT_ABILITY_MOD = 2;

export default class BaseUnit extends GridSprite {

  constructor(key, frame, player) {
    super(game, 0, 0, key, frame);
    this.id = ID++;
    this.playerId = player.id;
    this.setSelected(false);
    this.onMoveDone = new Phaser.Signal();

    this.abilities = [];
    this.preparedAbility = null;
    this.actionPoints = this.baseActionPoints;
    this.health = this.baseHealth = 100;

    this.shadow = game.add.sprite(0, 7, 'actors', 'shadow.png');
    this.shadow.anchor.set(0.5, 1);
    this.root.addChild(this.shadow);
    this.root.sendToBack(this.shadow);

    this.hpbar = new HpBar(this, 0, 0);
    this.root.addChild(this.hpbar);

    this.portraitFrameName = frame.split("_")[0] + "_prt.png";
  }

  seeAt(sprite) {
    this.base.scale.set(Math.sign((sprite.centerX || sprite.x) - this.centerX) || 1, 1);
  }

  receivePass(ball) {
    return this.base.animations.play('attack');
  }

  getAbilitiesCount(type) {
    let ability = this.abilities.find(ab => ab.type === type);
    if (ability && ability.count) return ability.count;
    return 0;
  }

  getFirstAvailableAbility(type) {
    return this.abilities.find(ab => ab.type === type && !ab.disabled);
  }

  addCooldownAbility(ability, cooldown) {
    this.abilities.push({ ...ability, afterUseCooldown: cooldown });
  }

  addOnetimeAbility(ability) {
    if (ability.type === "instant_heal") {
      return game.addTurnAnimation(this.healSelf())
    }
    let anotherAbility = this.abilities.find(ab => ab.type === ability.type);
    if (anotherAbility) {
      if (anotherAbility.afterUseCooldown !== undefined) return; //ignore one-time ability
      anotherAbility.count++;
    } else {
      this.abilities.push({ ...ability, count: 1 });
    }
  }

  prepareAbility(ability) {
    if (this.preparedAbility === ability) {
      this.preparedAbility = null
    } else {
      if (!ability && this.preparedAbility) {
        if (this._overwatchSprite) this._overwatchSprite.destroy();
      }
      this.preparedAbility = ability;
    }
  }

  afterTurn() {
    for (let ab of this.abilities) {
      if (ab.cooldown !== undefined) {
        ab.cooldown--;
        if (ab.cooldown <= 0) {
          ab.disabled = false;
          delete ab.cooldown;
        }
      }
    }
    if (this.preparedAbility) {
      if (this.preparedAbility.count !== undefined) {
        this.preparedAbility.count--;
        if (this.preparedAbility.count <= 0) {
          this.preparedAbility.deleted = true; //this is for UI
          this.abilities.splice(this.abilities.indexOf(this.preparedAbility), 1);
        }
      } else if (this.preparedAbility.afterUseCooldown) {
        this.preparedAbility.cooldown = this.preparedAbility.afterUseCooldown;
        this.preparedAbility.disabled = true;
      }
      if (!this.preparedAbility.tillNextTurn) {
        this.preparedAbility = null;
      }
    }
  }

  beforeSwitchFrom() {
    this.actionPoints = this.baseActionPoints;
  }

  beforeSwitchTo() {
    if (this.preparedAbility) {
      this.prepareAbility(undefined);
    }
  }

  reset() {
    this.beforeSwitchFrom();
    this.beforeSwitchTo();
  }

  getHitAction(ball, {ballPower, playerId}) {
    if (this.isOnOverwatch) {
      //console.log('overwatch - kick back');
      return {
        from: ball,
        ricochet: true,
        healthDrop: 34 * ballPower / 2 / this.armor,
        kickBack: true
      }
    }
    return {
      ricochet: true,
      healthDrop: 34 * ballPower / this.armor,
      shiftAt: 2,
      from: ball
    }
  }

  get armor() {
    return 1;
  }

  get isOnOverwatch() {
    return this.isPrepared("overwatch");
  }

  get canPassDirectly() {
    return true;
  }

  get passRadius() { return 1; }    ////////// ROMASHKA IS HERE

  get canGetDirectPass() {
    return !this.isBurning; //i think burning player cannot catch ball...
  }

  get kickRadius() {
    return 1 + (this.isPrepared("distant") ? 1 : 0);
  }

  get fireDamage() {
    return 5;
  }

  damageFire() {
    //todo: different damage
    this._lastHealthDrop = "fire";
    this.health -= this.fireDamage;
    this.checkHealth();
  }

  checkHealth() {
    if (this.health <= 0) {
      this.fall();
      let corpse = game.add.sprite(this.x, this.y, this.base.key, this.base.frameName);
      corpse.orderSortKey = this.orderSortKey;
      this.parent.add(corpse);
      this.kill();
      console.log('Im dead!', this, new Error().stack)
      corpse.anchor.set(0.5, 1);
      //todo: group
      corpse.lifespan = 2 * 1000;
      corpse.kill = function () {
        //???super.kill();
        Phaser.Sprite.prototype.kill.call(this);
        this.destroy();
      }
    }
  }

  healSelf() {
    game.sounder.play('heal');
    this.health = 100;
    return createHeal(this.x, this.y - this.base.height/2).animations.currentAnim.onComplete;
  }

  onHitAction(action) {
    console.log('hit by ball', action);
    if (action.kickBack) {
      game.addTurnAnimation(this.kick(action.from).onComplete)
      if (this.isOnOverwatch) {
        this.prepareAbility(undefined);
      }
    }
    if (action.from.isBurning) {
      game.logic.addFireToUnit(this, 6);
    }
    let heal = action.from.healMe(this)
    let healthDrop = action.healthDrop;
    if (heal) {
      healthDrop = 0;
      game.addTurnAnimation(this.healSelf());
    } else if (action.shiftAt) {
      let targetHex = game.grid.getHexAtVector(this.hex, this.x - action.from.x, this.y - action.from.y, action.shiftAt);
      /*if (targetHex.col === this.hex.col && targetHex.row === this.hex.row) {
        //do nothing
        //debugger;
      } else*/ {
        this.fall();
        const b = createBloodRand(this.x, this.y-this.base.height*0.8);
        b.scale.x = this.base.scale.x;
        let targetPoint = game.grid.getMiddlePoint(targetHex);
        const t = game.add.tween(this).to(targetPoint, 600, null, true);
        const oldy = this.base.y;
        this.base.y = -50;
        game.add.tween(this.base).to({ y: oldy }, 600, Phaser.Easing.Bounce.Out, true);
        t.onComplete.addOnce(() => this.stop());
        game.addTurnAnimation(t.promise());
        let hotOnWay = targetHex.flags.fire;

        t.onComplete.addOnce(() => {
          if (hotOnWay) game.logic.addFireToUnit(this, 6);
        })
        game.logic.relocateUnit(this, targetHex);
      }
    }
    if (healthDrop) {
      this._lastHealthDrop = "onHitAction";
      this.health -= healthDrop;
    }
    if (action.die) {
      this.health = 0;
    }

    this.checkHealth();
  }

  get canWalk() {
    return this.actionPoints >= this.walkCost;
  }

  get canKick() {
    return this.actionPoints >= this.kickCost;
  }

  applyAbilityCost(cost, actionType) {
    if (this.preparedAbility && this.preparedAbility.action !== undefined && this.preparedAbility.action === actionType) {
      return this.preparedAbility.cost;
    }
    return cost;
  }


  //more --> better
  get orderPriority() { return 0; }

  get missRadius() { return this.isPrepared("sniper") ? 0 : 1; }

  get baseActionPoints() { return 2; }

  get walkCost() { return this.applyAbilityCost(this.baseWalkCost, "move"); }

  get baseKickCost() { return 1; }

  get kickCost() {  return this.applyAbilityCost(this.baseKickCost, "kick"); }

  get baseWalkCost() { return 1; }

  get walkDistance() { return this.baseWalkDistance * (this.isPrepared("sprint") ? SPRINT_ABILITY_MOD : 1); } //toso[grishin]: instead each ability may have something like "distanceModifier"

  get baseWalkDistance() { return 6; }

  get kickDistance() {
    return (this.baseKickDistance + (this.isPrepared("sniper") ? 2 : 0) )
      * (this.isPrepared("x2") ? 2 : 1);
  }

  get baseKickPower() { return 1;}

  get kickPower() { return this.baseKickPower * (this.isPrepared("x2") ? 2 : 1)}

  isPrepared(type) {
    return this.preparedAbility && this.preparedAbility.type === type;
  }

  get baseKickDistance() { return 5; }

  get cellMoveTime() { return this.baseCellMoveTime * (this.isPrepared("sprint") ? 0.5 : 1) }

  get baseCellMoveTime() { return 300; }
  spendActionPoints(cost) {
    this.actionPoints -= cost;
  }

  get canFastKick() {
    return this.isPrepared("sprint");
  }

  get canKickOnRun() {
    return false;
  }

  canAbility(ability) {
    return ability.cost <= this.actionPoints;
  }

  get hasActionPoints() { return this.actionPoints > 0; }

  setSelected(flag) {
    if (flag) {
      this.base.filters = [new SelectionShader(this.selectionColor)]
    } else {
      this.base.filters = undefined;
    }
  }

  get selectionColor() {
    return this.playerId !== null ? game.logic.getPlayer(this.playerId).color : SELECTION_YELLOW;
  }

  moveByAction(action) {
    this._moveAction = action;
    return this.followPath(game.grid.findPath(action.from, action.to)).then(() => {
      this._moveAction = undefined;
    })
  }

  followPath(hexes) {
    this.hexesToMove = hexes;
    this.nextMove = null;
    return this.onMoveDone.promise();
  }

  kick(ball, shallBurnKicker = true) {
    this.seeAt(ball);
    if (ball.isBurning && shallBurnKicker) {
      game.logic.addFireToUnit(this, 6);
    }
    if (this.base.animations.getAnimation('attack')) {
      return this.base.animations.play('attack');
    }
  }

  walk() {
    if (this.base.animations.getAnimation('walk')) {
      this.base.animations.play('walk');
    }
  }

  stop() {
    if (this.base.animations.getAnimation('idle')) {
      this.base.animations.play('idle');
    }
  }

  fall() {
    if (this.base.animations.getAnimation('die')) {
      this.base.animations.play('die');
    }
  }

  overlap(unit) {
    if (unit.collider) {
      return this.collider.overlap(unit.collider);
    }
    return this.collider.overlap(unit);
  }

  update() {
    super.update();
    this.hpbar.update();
    this.hpbar.onHpChanged();
    (this.base.filters ||[]).forEach(f => f.update());
    for (let ab of this.abilities) {
      ab.disabled = ab.cost > this.actionPoints || (ab.cooldown !== undefined && ab.cooldown > 0);
    }
    let bonusOnWay = game.logic.bonuses.find(b => b.overlap(this));
    if (bonusOnWay) {
      bonusOnWay.onKickBy(this);
    }
    if (this.hexesToMove) {

      if (this._moveAction) {
        let ki = this._moveAction.ballKicks.length - 1;
        for (; ki >= 0; ki--) {
          let bk = this._moveAction.ballKicks[ki];
          if (this.overlap(bk.ball)) {
            game.addTurnAnimation(bk.ball.flyByKick(bk));
            this._moveAction.ballKicks.splice(ki, 1);
          }
        }
      }


      if (this.isMoving) return;
      this.nextMove = this.hexesToMove.shift();
      if (!this.nextMove) {
        this.hexesToMove = undefined;
        this.onMoveDone.dispatch();
        return;
      }
      let p = this.getPoint(this.nextMove);
      let hotOnWay = game.grid.getHexBy(this.nextMove.col, this.nextMove.row).flags.fire;
      //todo: play anim
      this.isMoving = true;
      this.seeAt(p);
      game.add.tween(this).to(p, this.cellMoveTime, null, true).onComplete.addOnce(() => {
        if (hotOnWay) {
          game.logic.addFireToUnit(this, 6);
        }
        this.isMoving = false;
      });


    }
  }

  destroy() {
    this.hex.removeUnit(this);
    super.destroy();
  }


  get orderSortKey() {
    return this.orderPriority * 100000 + this.id;
  }


  animatePreparedAction() {
    if (!this.preparedAbility) {
      return;
    }
    if (this.preparedAbility.type === "overwatch") {
      let sprite = game.add.sprite(0, -this.base.height, "ui", "icon_watch.png");
      this.root.addChild(sprite);
      sprite.anchor.set(0.5, 0.5);
      this._overwatchSprite = sprite;
      game.add.tween(this._overwatchSprite).to({y: sprite.y-5}, 1000, Phaser.Easing.Cubic.InOut, true, 0, -1, true);
      return;
    }
    let sprite = game.add.sprite(0, -this.base.height, ...this.preparedAbility.sprite);
    this.root.addChild(sprite);
    //sprite.tint = 0xcccccc;
    //sprite.alpha = 0.8;
    sprite.anchor.set(0.5, 0.5);
    //sprite.scale.set(0.5, 0.5);
    game.add.tween(sprite).to({y: sprite.y-100, alpha: 0}, 1000, null, true).onComplete.addOnce(() => sprite.destroy())
  }

  get kickAngle() {
    return Math.PI/3;
  }
}
