import BaseUnit from "./baseunit";
import {HEAL} from "../../objects/abilities"

export default class Wizzard extends BaseUnit {
  constructor(player) {
    super("actors", "wizard_i.png", player);
    this.base.anchor.x = 0.4;
    this.base.anchor.y = 0.8;
    this.base.y = -16;
    this.base.animations.add('idle', ['wizard_i.png'], 18, true);
    this.walkAnim = this.base.animations.add('walk', ['wizard_w1.png', 'wizard_w2.png', 'wizard_w3.png', 'wizard_w4.png', 'wizard_w5.png'], 18, true);
    this.walkAnim.onLoop.add(() => {game.sounder.play('step')}, this);

    this.base.animations.add('attack', ['wizard_a1.png', 'wizard_a2.png',
      'wizard_a3.png', 'wizard_a2.png', 'wizard_a3.png',
      'wizard_a2.png', 'wizard_a3.png', 'wizard_i.png'], 18);
    this.base.animations.add('die', ['wizard_d.png'], 12);

    this.addCooldownAbility(HEAL, 2);
  }

  get baseWalkDistance() { return 4; }
  get baseKickDistance() { return 6; }
  //get missRadius() { return this.isPrepared("heal") ? 0 : 1; }

  get orderPriority() { return 5; }

  get baseActionPoints() { return 3; }
}
