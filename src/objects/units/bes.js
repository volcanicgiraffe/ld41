import BaseUnit, {ATTACK_ANIM_LENGTH} from "./baseunit";
import {SPRINT} from "../../objects/abilities"

export default class Bes extends BaseUnit {
  constructor(player) {
    super("actors", "bes_i.png", player);
    this.base.x = 0;
    this.base.animations.add('idle', ['bes_i.png'], 18, true);
    this.walkAnim = this.base.animations.add('walk', ['bes_w1.png','bes_w2.png','bes_w3.png','bes_w4.png'], 18, true);
    this.walkAnim.onLoop.add(() => {game.sounder.play('step_d')}, this);
    this.base.animations.add('attack', ['bes_a1.png'].concat(new Array(ATTACK_ANIM_LENGTH)).fill('bes_a2.png',1).concat('bes_i.png'), 18);
    this.base.animations.add('die', ['bes_d.png'], 12);
    this.addCooldownAbility(SPRINT, 1);

  }

  get baseCellMoveTime() { return 220; }

  get orderPriority() { return 3; }
}
