import BaseUnit, { ATTACK_ANIM_LENGTH } from "./baseunit";
import {X2} from "../abilities";

export default class knight extends BaseUnit {
  constructor(player) {
    super("actors", "knight_i.png", player);
    this.base.x = 4;
    this.base.y = 2;
    this.base.animations.add('idle', ['knight_i.png'], 18, true);
    this.walkAnim = this.base.animations.add('walk', ['knight_w1.png', 'knight_w2.png', 'knight_w3.png', 'knight_w4.png', 'knight_w5.png'], 18, true);
    this.walkAnim.onLoop.add(() => {game.sounder.play('step_h')}, this);

    this.base.animations.add('attack', ['knight_a1.png'].concat(new Array(ATTACK_ANIM_LENGTH)).fill('knight_a2.png', 1).concat('knight_i.png'), 18);
    this.base.animations.add('die', ['knight_d.png'], 12);

    this.addCooldownAbility(X2, 1);
  }

  get baseWalkDistance() { return 3; }
  get baseKickDistance() { return 7; }

  get armor() { return 2; }

  get fireDamage() {
    return 2;
  }


  get orderPriority() { return 2; }

  get kickAngle() {
    return Math.PI/9;
  }

}
