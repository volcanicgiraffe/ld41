import { createFire, createBloodRand, createMeatChunks, createConfettiRand } from "../effects/effectsManager"

export default class Fan extends Phaser.Sprite {
    constructor(x, y, grp) {
        const ind = game.rnd.integerInRange(1, 2);
        super(game, x, y, "field_elements", `fan${ind}_i1.png`);
        this.ind = ind;
        this.playerId = this.ind-1;
        this.anchor.set(0.5, 1);

        this.recreateAnims();
        this.idle = true;
        this.cheerTimerInterval = game.rnd.integerInRange(3000, 5000);
        this.cheerTimer = this.cheerTimerInterval;
        grp.add(this);
        this.resTimer = 5000;
        this.frwTimer = Math.random() * 1000;
    }

    recreateAnims() {
        //this.animations.destroy();
        this.animations.add('cheer', [
            this.getFrame('i2'), this.getFrame('i3'),
            this.getFrame('i2'), this.getFrame('i3'),
            this.getFrame('i2'), this.getFrame('i3'),
            this.getFrame('i1')], 12);
        this.animations.add('hooray2', [
            this.getFrame('i2'), this.getFrame('i3')], 12, true);
        this.animations.add('hooray', [
            this.getFrame('ye1'), this.getFrame('ye2')], 12, true);
    }

    loseAllShit(winPlayer) {
        this.idle = false;
        const coin = Math.random();
        const shallHit = (this.playerId === undefined || !winPlayer) ? (coin < 0.1) : (this.playerId === winPlayer.id);
        if (shallHit) {
            this.frameName = this.getFrame('hit');
        } else if (coin < 0.3) {
            this.animations.play('hooray2');
        } else {
            this.animations.play('hooray');
        }
    }

    onHit() {
        this.frameName = this.getFrame('hit');
        // TODO splatter;
    }

    calmTheFuckDown() {
        this.idle = true;
        this.animations.stop();
        this.frameName = this.getFrame('i1');
    }

    getFrame(v) {
        return `fan${this.ind}_${v}.png`;
    }

    dieBy(ball) {
        if (!this.alive || this._fire) return;
        if (this.ind !== 'S') {
            this.ind = 'S';
            this.playerId = undefined;
            this.frameName = this.getFrame('i1');
            this.recreateAnims();
            createMeatChunks(this.x - 200, this.y - 84)
            if (ball.isBurning) {
                this._fire = createFire(0, -this.height);
                this._fire.scale.set(2, 2);
                this.addChild(this._fire);
                this.animations.play('hooray');
                game.time.events.add(3000, () => {
                    this._fire.destroy();
                    this._fire = null;
                });
            } else {
                this._blood = createBloodRand(this.x - 200, this.y - 84);
            }
        } else {
            if (this.resTimer <= 0)
                this.kill();
        }
    }

    update() {
        if (this.ind === 'S' && this.resTimer > 0) {
            this.resTimer -= game.time.physicsElapsedMS;
        }
        if (!this.idle) {
            this.frwTimer -= game.time.physicsElapsedMS;
            if (this.frwTimer <= 0) { 
                this.frwTimer = 500*Math.random() + 1000;
                if(Math.random() > 0.3) {
                    createConfettiRand(this.x - 200, this.y - 64)
                }
            }
            return;
        }
        this.cheerTimer -= game.time.physicsElapsedMS;
        if (this.cheerTimer <= 0) {
            this.cheerTimer = this.cheerTimerInterval;
            this.animations.play('cheer');
        }
    }
}