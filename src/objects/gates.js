export default class Gates extends Phaser.Sprite {
  constructor(player, mirrored = false) {
    super(game, 0, 0, "actors", "gates.png");

    this.playerId = player.id;
    this.anchor.set(0.5, 0.5);
    if (mirrored) this.scale.set(-1, 1);

    this.mirrorX = mirrored ? -1 : 0;
  }

  get pattern() {
    return [
      {col: 0, row: -2, goalpost: true},
      {col: 0, row: 2, goalpost: true},
      {col: this.mirrorX, row: -1, goal: true},
      {col: 0, row: 0, goal: true},
      {col: this.mirrorX, row: 1, goal: true},
    ];
  }

  get type() {
    return "gates";
  }

  get sortY() {
    return this.y + this.height / 2;
  }
}
