export default class HpBar extends Phaser.Sprite {
  constructor(target, x, y, barWidth = target.width) {
    super(game, x, y);
    this.anchor.set(0.5, 0.5);
    this.alpha = 0;

    this.dx = x;
    this.dy = y;

    this._bitmap = game.add.bitmapData(barWidth, 4);
    this.loadTexture(this._bitmap);

    this._target = target;
    this._oldHealth = this._target.health;
    this._cooldown = 0;

    this._redrawBar();
  }

  update() {
    if (this._cooldown <= 0) return;

    this._cooldown -= game.time.physicsElapsedMS;
    if (this._cooldown < 0) this._cooldown = 0;
    if (this._cooldown < FADE_TIME) this.alpha = this._cooldown / FADE_TIME;
  }

  onHpChanged() {
    if (this._oldHealth === this._target.health) return;

    this._oldHealth = this._target.health;
    //this._cooldown = VISIBLE_TIME + FADE_TIME;
    this.alpha = 1;
    this._redrawBar();
  }

  _redrawBar() {
    // TODO: assets
    this._bitmap.rect(0, 0, this._bitmap.width, 4, "rgb(250, 68, 80)");
    this._bitmap.rect(0, 0, this._target.health / this._target.baseHealth * this._bitmap.width, 4, "rgb(68, 250, 80)");
    this._bitmap.dirty = true;
  }
}