import { HEX_MIDDLE_HEIGHT, HEX_WIDTH } from "../model/grid";
import {createFire} from "../effects/effectsManager"

export default class GridSprite extends Phaser.Sprite {

  constructor(game, x, y, key, frame) {
    super(game, x, y);
    this.hex = undefined;
    this.root = game.add.group();
    this.addChild(this.root);
    this.base = game.add.sprite(0, 0, key, frame);
    this.root.addChild(this.base);
    this.base.anchor.set(0.5, 1);

    this.collider = game.add.sprite(0, 0);
    this.collider.anchor.set(0.5, 1);
    this.root.addChild(this.collider);
    this.collider.width = HEX_WIDTH;
    this.collider.height = HEX_MIDDLE_HEIGHT;

    this.effects = [];
  }

  //get collider() { return this.root; }

  get isOnGrid() {
    return this.hex !== undefined;
  }

  locate() {
    let { x, y } = this.getPoint(this.hex);
    this.x = x;
    this.y = y;
    return this;
  }

  getPoint(hex) {
    let { x, y } = game.gridview.getMiddlePoint(hex);
    return { x, y: y + (HEX_MIDDLE_HEIGHT / 2) - 7 };

  }

  get sortY() {
    return this.y;
  }


  _addFire() {
    if (this._fire) return;
    this._fire = createFire(0, -this.base.height);
    this._fire.scale.set(2,2);
    this.root.addChild(this._fire);
  }

  get fireDamage() {
    return 10;
  }

  damageFire() {}

  _removeFire() {
    if (!this._fire) return;
    this._fire.destroy();
    this._fire = undefined;
  }

  get isBurning() { return !!this._fire; }


  hasEffect(type) {
    return this.effects.some(ef => ef.type === type);
  }

  getEffect(type) {
    return this.effects.find(ef => ef.type === type);
  }

  removeEffect(type) {
    this.effects = this.effects.filter(ef => ef.type !== type);
    this.onRemoveEffect(type);
  }

  addEffect(effect, updateCooldown = undefined) {
    if (this.hasEffect(effect.type)) {
      if (updateCooldown !== undefined) {
        let ef = this.getEffect(effect.type);
        ef.cooldown = updateCooldown;
      }
      return;
    }
    this.effects.push(effect);
    this.onAddEffect(effect.type);
  }

  onAddEffect(type) {
    switch (type) {
      case "fire":
        this._addFire();
        break;
      default:
        console.warn("add uncknown effect", type, "to", this);
    }
  }

  onRemoveEffect(type) {
    switch (type) {
      case "fire":
        this._removeFire();
        break;
      default:
        console.warn("remove uncknown effect", type, "to", this);
    }
  }

  updateEffects() {
    for (let ef of this.effects.slice()) {
      if (ef.cooldown !== undefined) {
        if (ef.type === "fire" && this.hex.flags.fire) {
          //keep as is
        } else {
          ef.cooldown--;
          if (ef.cooldown === 0) {
            this.removeEffect(ef.type);
          }
        }
      }
    }
    if (this.hasEffect("fire")) {
      this.damageFire();
    }
  }

}