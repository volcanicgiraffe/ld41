import { HEX_MIDDLE_HEIGHT, HEX_WIDTH } from "../model/grid";
import { createLavaFlick } from '../effects/effectsManager';

const TYPES = [
  {
    frame: 'lavapit1.png', cx: -HEX_WIDTH - 3, cy: -HEX_MIDDLE_HEIGHT, pattern: `
       ###
       ####
    `, flags: { fire: true, passable: true }
  },
  {
    frame: 'lavapit2.png', cx: -HEX_WIDTH, cy: -HEX_MIDDLE_HEIGHT + 5, pattern: `
       ##
       ##
       #.
    `, flags: { fire: true, passable: true }
  },
  {
    frame: 'lavapit3.png', cx: -HEX_WIDTH / 2, cy: -HEX_MIDDLE_HEIGHT, pattern: `
       ####
       .##.
    `, flags: { fire: true, passable: true }
  },
  {
    frame: 'stone1.png', cx: -HEX_WIDTH / 2, cy: -HEX_MIDDLE_HEIGHT, pattern: `
       ##.
       .##
    `, flags: { solid: true }
  },
  {
    frame: 'stone2.png', cx: -HEX_WIDTH, cy: -HEX_MIDDLE_HEIGHT, pattern: `
       ##
       ##
    `, flags: { solid: true }
  },
];


export default class Obstacle extends Phaser.Sprite {
  constructor(col, row, frame) {
    super(game, 0, 0, "field_elements", "lavapit1.png");

    this.col = col;
    this.row = row;
    this.timerAnim = Math.random() * 1000;

    this.kind = frame ? TYPES.find(st => st.frame === frame) : game.rnd.pick(TYPES);
    if (!this.kind) throw new Error("unknown frame name: " + frame + ", known are: " + TYPES.map(t => '"' + t.frame + '"').join(' '));
    this.frameName = this.kind.frame;

    this._pattern = [];
    this.kind.pattern.split('\n').map(l => l.trim()).filter(l => l.length).forEach((row, ri) => {
      row.trim().split('').forEach((chr, ci) => {
        switch (chr) {
          case '#':
            this._pattern.push({ col: ci, row: ri });
            break;
        }
      });
    });


    this.anchor.set(0, 0);

    let point = game.gridview.getMiddlePoint(this);

    this.x = point.x + this.kind.cx;
    this.y = point.y + this.kind.cy;
  }

  update() {
    if (this.flags.fire) {
      this.timerAnim -= game.time.physicsElapsedMS;
      if (this.timerAnim <= 0) {
        this.timerAnim = Math.random() * 3000 + 500;

        let cell = game.rnd.pick(this._pattern);
        let { x, y } = game.gridview.getMiddlePoint({ col: this.col + cell.col, row: this.row + cell.row });

        createLavaFlick(x, y - HEX_MIDDLE_HEIGHT);
      }
    }
  }

  get flags() {
    return this.kind.flags;
  }

  get pattern() {
    return this._pattern;
  }

  get type() {
    return "obstacle";
  }

  get sortY() {
    return this.flags.passable ? -10 : this.y;
  }
}
