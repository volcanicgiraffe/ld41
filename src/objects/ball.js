import GridSprite from "./gridsprite";
import {showSelection} from "../effects/selection.js"
import { createHit, createLoopHeal, createSlimeEffect } from 'effects/effectsManager';

export default class Ball extends GridSprite {

  constructor(x, y) {
    super(game, x, y, "actors", "ball.png");

    this.shadow = game.add.sprite(0, 7, 'actors', 'shadow.png');
    this.shadow.anchor.set(0.5, 1);
    this.root.addChild(this.shadow);
    this.root.sendToBack(this.shadow);

    this.pupilTimer = 2000;
    this.blinkTimer = 1000;

    this.eye = game.add.sprite(0, -this.base.height * 0.5, 'actors', 'ball_eye.png');
    this.eye.anchor.setTo(0.5);
    this.base.addChild(this.eye);
    this.pupil = game.add.sprite(0, 0, 'actors', 'ball_pupil_b.png');
    this.pupil.anchor.setTo(0.5);
    this.gloss = game.add.sprite(-6, -6, 'actors', 'ball_eye_gloss.png');
    this.gloss.anchor.setTo(0.5);
    this.eye.addChild(this.pupil);
    this.eye.addChild(this.gloss);

    this.base.animations.add('squish_h', ['ball_bh1.png', 'ball_bh2.png', 'ball_bh1.png', 'ball.png'], 12);
    this.base.animations.add('squish_v', ['ball_bv1.png', 'ball_bv2.png', 'ball_bv1.png', 'ball.png'], 12);
    this.closeEye();
    this.onUnitHit = () => { };

    this._rect = new Phaser.Circle(this.left, this.top, this.base.width);
  }

  get rect() {
    this._rect.x = this.left + this.parent.x;
    this._rect.y = this.top + this.parent.y - this.base.height/2;
    return this._rect;
  }

  closeEye() {
    this.eye.frameName = 'ball_eye_closed.png';
    this.pupil.visible = false;
    this.gloss.visible = false;
  }

  openEye() {
    this.eye.frameName = 'ball_eye.png';
    this.pupil.visible = true;
    this.gloss.visible = true;
  }

  blink() {
    this.closeEye();
    game.time.events.add(300, () => this.openEye());
    this.blinkTimer = 2000 * Math.random() + 3000;
  }

  lookTo(x, y) {
    const v = new Phaser.Point(x - this.x, y - this.y).normalize();
    const pwr = Math.random() * 1 + 4;
    game.add.tween(this.pupil).to({ x: v.x * pwr, y: v.y * pwr }, 300, Phaser.Easing.Default, true);
  }

  animateHit() {
    createHit(this.x, this.y - this.base.height * 0.5);
    this.base.animations.play('squish_v');
    this.base.y = -20;
    game.add.tween(this.base).to({ y: 0 }, 600, Phaser.Easing.Bounce.Out, true);
    if (this.eye.frameName === 'ball_eye_closed.png') {
      this.openEye();
      this.pupil.frameName = 'ball_pupil.png';
      this.pupil.x = 0;
      this.pupil.y = 0;
    } else if (Math.random() < 0.5) {
      this.closeEye();
    } else {
      this.pupil.frameName = 'ball_pupil.png';
    }
  }

  endAnimateHit() {
    this.openEye();
    this.pupil.frameName = 'ball_pupil_b.png';
  }

  flyDirectly(from, to) {
    let toxy = this.getPoint(to);
    return game.add.tween(this).to(toxy, 300, null, true).promise();
  }

  flyByKick(action) {
    action.unit.kick(action.ball, action.shallBurnKicker);
    if (action.unit.isPrepared("x2")) {
      game.sounder.play('kick_hard');
    } else {
      game.sounder.play('kick');
    }
    this.action = action;
    if (this.action.burning) {
      game.logic.addFireToUnit(this, 8);
    }
    if (this.action.effect) {
      this.addEffect(this.action.effect);
    }
    this.animateHit();
    this.flyPromise = new Promise((resolve) => this.flyResolve = () => { this.action = null; this.flyTween = null; resolve() });
    let toxy = this.getPoint(action.to);
    this.flyTween = game.add.tween(this).to(toxy, 600, null, true);
    let afterComplete = (() => this.flyResolve());
    this.flyAwaiting = [];
    if (action.ricochet) {
      let ricochetTo = this.getPoint(action.afterRicochet);
      if (action.ricochetAtEnd) {
        if (ricochetTo) {
          afterComplete = () => {
            this.flyTween = game.add.tween(this).to(ricochetTo, 300, null, true).promise().then(() => this.flyResolve());
          }
        }
      } else {
        this.flyAwaiting.push({
          unit: action.ricochet,
          ricochetTo: ricochetTo
        });
      }
    }
    this.flyTween.promise().then(afterComplete);
    for (let hu of action.unitHits) {
      this.flyAwaiting.push(hu);
    }
    if (action.goal) {
      this.flyAwaiting.push({ hex: action.goal, goal: true });
    }
    this.flyAwaiting = this.flyAwaiting.reverse();
    if (action.passTo) {
      action.passTo.seeAt(this);
    }
    return this.flyPromise.then(() => {
      this.endAnimateHit();
    });
  }

  update() {
    (this.base.filters ||[]).forEach(f => f.update());

    this.pupilTimer -= game.time.physicsElapsedMS;
    if (this.pupilTimer <= 0) {
      // todo look at closest enemy?
      this.lookTo(Math.random() * 1000, Math.random() * 800);
      this.pupilTimer = 2000;
    }

    if (this.eye.frameName === 'ball_eye.png') {
      this.blinkTimer -= game.time.physicsElapsedMS;
      if (this.blinkTimer <= 0) {
        this.blink();
      }
    }

    let bonusOnWay = game.logic.bonuses.find(b => b.overlap(this));
    if (bonusOnWay) {
      bonusOnWay.onKickBy(this);
    }

    let hex = game.gridview.getHexBy(this.x, this.y);
    if (hex && hex.flags.fire) {
      game.logic.addFireToUnit(this, 6);
    }

    if (this.flyTween) {
      let i = this.flyAwaiting.length - 1;
      for (; i >= 0; i--) {
        let aw = this.flyAwaiting[i];
        if (aw.goal && Phaser.Point.distance(this.getPoint(aw.hex), this) < 20) {
          //todo: signal to level from here
          this.flyAwaiting.splice(i, 1);
        } else if (aw.unit && aw.unit.overlap && aw.unit.overlap(this)) {
          if (aw.ricochetTo) {
            this.flyTween.stop();
            this.flyTween = game.add.tween(this).to(aw.ricochetTo, 300, null, true).promise().then(() => this.flyResolve());
          }
          if (aw.hitAction) {
            aw.unit.onHitAction(aw.hitAction);
            this.onUnitHit();
          }

          this.flyAwaiting.splice(i, 1);
        }
      }
      if (this.action && this.action.passTo && Phaser.Point.distance(this, this.action.passTo) < 50) {
        game.addTurnAnimation(this.action.passTo.receivePass(this).onComplete);
        this.action.passTo = null;
      }
    }
  }

  healMe(unit) {
    if (this.hasEffect("healing") && unit.playerId === this.getEffect("healing").unit.playerId) {
      this.removeEffect("healing");
      return 10;
    } else {
      return false;
    }
  }


  setSelected(flag, doBlink = false) {
    showSelection(this.base, doBlink ? {r: 0, g: 255, b: 255} : { r: 255, g: 0, b: 0 }, doBlink ? 0.5 : 0, flag);
  }

  get type() {
    return "ball";
  }


  onAddEffect(type) {
    switch (type) {
      case "fire":
        this._addFire();
        break;
      case "healing":
        this._healEffect = createLoopHeal(0, -this.base.height*0.6);
        this.addChild(this._healEffect);
        break;
      case "slime":
        //todo:
        this._slimeEffect = createSlimeEffect(0, 0);
        this.addChild(this._slimeEffect);
        break;

      default:
        console.warn("add uncknown effect", type, "to", this);
    }
  }

  onRemoveEffect(type) {
    switch (type) {
      case "fire":
        this._removeFire();
        break;
      case "healing":
        this._healEffect.destroy();
        break;
      case "slime":
        this._slimeEffect.destroy();
        break;
      default:
        console.warn("remove uncknown effect", type, "to", this);
    }
  }
}
