import Fan from './fan';
const SPACING = 40;

export default class FanManager {
    constructor(grp) {
        this.fans3 = [];
        for (let i = 0; i < 10; i++) {
            this.fans3.push(new Fan(650 + (SPACING * 1.2 * i) + game.rnd.integerInRange(1, 20), 26, grp))
        }
        this.fans2 = [];
        for (let i = 0; i < 18; i++) {
            this.fans2.push(new Fan(580 + (SPACING * i) + game.rnd.integerInRange(1, 10), 66, grp))
        }
        this.fans1 = [];
        for (let i = 0; i < 24; i++) {
            this.fans1.push(new Fan(500 + SPACING * i, 106, grp))
        }
    }

    iterateAll(func) {
        this.fans1.forEach(f => func(f));
        this.fans2.forEach(f => func(f));
        this.fans3.forEach(f => func(f));
    }

    onGoal(winPlayer) {
        this.iterateAll((f) => f.loseAllShit(winPlayer));
        game.time.events.add(6000, () => {
            this.iterateAll((f) => f.calmTheFuckDown())
        });
    }

    onSmash() {
        this.iterateAll((f) => Math.random() < 0.2 && f.loseAllShit());
        game.time.events.add(3000, () => this.iterateAll((f) => f.calmTheFuckDown()));
    }
}