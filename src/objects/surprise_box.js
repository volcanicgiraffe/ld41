import GridSprite from './gridsprite';
import {dropAbility} from './abilities';
import {createBlueExplosion} from '../effects/effectsManager'

export default class SurpriseBox extends GridSprite {
  constructor(ability = dropAbility()) {
    super(game, 0, 0, "actors", "bonus.png");
    this.ability = ability;
  }

  onKickBy(someone) {
    game.logic.onBonusGet(someone, this);
    this.destroy();
    game.addTurnAnimation(createBlueExplosion(this.x, this.y).animations.getAnimation('bexp'));
  }

  get type() {
    return "bonus";
  }
}