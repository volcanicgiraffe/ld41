import GridSprite from "./gridsprite";

export default class Goalpost extends GridSprite {
  constructor() {
    let bmd = game.add.bitmapData(32, 32);
    bmd.rect(0, 0, 32, 32, 'rgb(255,0,0)');
    super(game, 0, 0, bmd); // DON'T WORRY, this sprite is invisible.
    this.alpha = 0;
    this.anchor.set(0.5);
  }

  get solid() {
    return true;
  }

  get type() {
    return "goalpost";
  }

}
