import HexGrid from "./grid";
import { SPRINT_ABILITY_MOD } from "../objects/units/baseunit";
import { FIRE, SPRINT, X2 } from "../objects/abilities";

export default class PlayerAI {
  constructor() {
  }

  set player(value) {
    this._player = value;
  }

  // TODO: icxon, works for current unit only, since uses canWalk (view prop)
  performBestAction(unit) {
    game.logic.prepareAbility(undefined, unit);
    this._fillGridWeights(unit);

    if (this._checkBallKickAction(unit)) return;
    if (this._checkFastKick(unit)) return;

    game.logic.moveUnitTo(unit, this._bestWalkableCell(unit));
  }

  _fillGridWeights(unit) {
    let player = game.logic.getPlayer(unit.playerId);
    let playerGatesHex = player.gates.hex;
    let opponentGatesHex = player.opponent.gates.hex;

    game.grid.forEach(hex => {
      game.logic.balls.forEach(ball => {
        hex._aiMoveWeigth = HexGrid.getDistance(hex, ball.hex);
      });

      hex._aiMoveWeigth += HexGrid.getDistance(hex, playerGatesHex) / 1000;
      hex._aiKickWeight = HexGrid.getDistance(hex, opponentGatesHex);

      if (hex.flags.fire) hex._aiMoveWeigth += 10;
      if (hex.bonus !== undefined) {
        let countX2 = unit.getAbilitiesCount(X2.type);
        let countFire = unit.getAbilitiesCount(FIRE.type);
        if (countX2 === 0) hex._aiMoveWeigth -= 4;
        if (countFire === 0) hex._aiMoveWeigth -= 4;
        // TODO: low hp
      }
    });


  }

  _checkBallKickAction(unit) {
    let ball = this._closestBallTo(unit.hex);
    if (ball === null || HexGrid.getDistance(unit.hex, ball.hex) !== 1) return false;

    // todo: icxon, check if need to apply (kickDistance * 1 < distanceToGate < kickDistance * 2)
    let ability = unit.getFirstAvailableAbility(X2.type);
    if (ability) game.logic.prepareAbility(ability);

    if (!ability) {
      // todo: fire apply condition?
      ability = unit.getFirstAvailableAbility(FIRE.type);
      if (ability) game.logic.prepareAbility(ability);
    }

    game.logic.getKickablePoints(unit, ball).forEach(hex => {
      hex.flags.canKick = true;
      if (game.logic.findUnitToGetPass(hex, unit)) {
        hex.flags.canPass = true;
      }
    });

    let bestKick = this._bestKickCell(unit);

    if (unit.canKick && bestKick !== null && bestKick.flags.canKick) {

      game.logic.kickBallTo(unit, ball, bestKick);
      return true;
    }
    return false;
  }

  _checkFastKick(unit) {
    if (Math.random() < 0.6) return false;

    let ability = unit.getFirstAvailableAbility(SPRINT.type);
    if (!ability) return false;

    let ball = this._closestBallTo(unit.hex);

    // don't sprint if ball is to close and we have some spare ap
    if (ball === null || (HexGrid.getDistance(unit.hex, ball.hex) <= unit.baseWalkDistance && unit.actionPoints > unit.walkCost)) return false;

    if (HexGrid.getDistance(unit.hex, ball.hex) > unit.baseWalkDistance * SPRINT_ABILITY_MOD) return false;

    game.logic.prepareAbility(ability);
    game.logic.moveUnitTo(unit, ball.hex);

    return true;
  }

  _bestKickCell(unit) {
    let best = null;
    let min = Number.MAX_VALUE;

    game.grid.forEach(hex => {
      if (hex.flags.canKick && hex._aiKickWeight !== undefined && hex._aiKickWeight < min) {
        best = hex;
        min = hex._aiKickWeight;
      }
    });

    return best;
  }

  _bestWalkableCell(unit) {
    let best = this._randomWalkableCell(unit);
    let min = Number.MAX_VALUE;

    game.grid.forEach(hex => {
      if (hex.flags.canWalk && hex.isFreeToStand && hex._aiMoveWeigth !== undefined && hex._aiMoveWeigth < min) {
        best = hex;
        min = hex._aiMoveWeigth;
      }
    });

    if (!best) best = unit.hex;

    return best;
  }

  _randomWalkableCell(unit) {
    let walkable = [];

    game.grid.forEach(hex => {
      if (hex.flags.canWalk && hex.isFreeToStand) walkable.push(hex);
    });

    return game.rnd.pick(walkable);
  }

  _closestBallTo(hex) {
    let closest = null;
    let min = Number.MAX_VALUE;
    game.logic.balls.forEach(ball => {
      if (HexGrid.getDistance(hex, ball.hex) < min) {
        min = HexGrid.getDistance(hex, ball.hex);
        closest = ball;
      }
    });
    return closest;
  }
}
