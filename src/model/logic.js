import Goalpost from "../objects/goalpost";
import { SPRINT, OVERWATCH_PSEUDO_ABILITY, SKIP_PSEUDO_ABILITY } from "../objects/abilities";

export class MoveAction {
  constructor(unit, from, to) {
    this.type = "move";
    this.unit = unit;
    this.from = from;
    this.to = to;
    this.canFastKick = unit.canFastKick;
    this.ballKicks = [];
  }

  addBallKick(action) {
    this.ballKicks.push(action);
  }

  get target() {
    return this.to;
  }
}

export class FreeMoveAction extends MoveAction {
  constructor(unit, from, to) {
    super(unit, from, to);
    this.type = "free_move";
  }
}

export class SkipUnit {
  constructor(unit) {
    this.type = "skip";
    this.unit = unit;
    this.overwatch = false;
  }

  setOverwatch() {
    this.overwatch = true;
    return this;
  }
}

export class KickAction {
  constructor(unit, ball, from, to) {
    this.type = "kick";
    this.unit = unit;
    this.ball = ball;
    this.ballPower = unit.kickPower;
    this.from = from;
    this.to = to;
    this.burning = false;
    this.playerId = unit.playerId;
    //put here other data - richochetes, units to damage, etc.
    this.passTo = undefined;
    this.unitHits = [];
    this.shallBurnKicker = game.grid.getDistance(unit.hex, from) <= 1
    this.effect = undefined;
  }

  setPassTo(unit) {
    this.passTo = unit;
  }

  setBurning(val) {
    this.burning = val;
  }

  addEffect(effect) {
    this.effect = effect;
  }


  get target() {
    return this.afterRicochet || this.to;
  }

  setGoalAt(goal) {
    this.goal = goal;
  }

  setRicochetAt(unit, target) {
    this.ricochet = unit;
    this.ricochetAtEnd = (unit.row === this.to.row && unit.col === this.to.col);
    this.afterRicochet = target;
  }

  setUnitHit(unit, cell, hitAction) {
    this.unitHits.push({ unit, cell, hitAction });
  }
}


export default class Logic {

  constructor(grid, gridView, group) {
    this.grid = grid;
    this.gridView = gridView;

    this.players = [];
    this.balls = [];
    this.bonuses = [];

    this.group = group;

    this.movementOrder = [];
    this.currentStep = undefined;

    this.onSwitch = new Phaser.Signal();
    this.onActionDone = new Phaser.Signal();
    this.onAbilityChanged = new Phaser.Signal();
  }

  forEachUnit(cb) {
    this.players.forEach(p => p.units.forEach(cb));
  }



  addPlayer(player) {
    this.players.push(player);
    this.movementOrder = [];
    this.updateMovementOrder();
    return player;
  }

  addFireToUnit(unit, fireCooldown = 1) {
    game.sounder.play('crust');
    unit.addEffect({type: "fire", cooldown: fireCooldown}, fireCooldown);
  }

  iterateEffects() {
    this.forEachUnit(unit => {
      unit.updateEffects();
    });
    this.balls.forEach(ball => {
      ball.updateEffects();
    })
  }

  addUnit(unit, hexlike) {
    let cell = this.grid.getHexBy(hexlike.col, hexlike.row);
    if (!cell || !cell.isFreeToStand) throw new Error("Cannot stand here");
    this.getPlayer(unit.playerId).units.push(unit);
    cell.addUnit(unit);
    this.group.add(unit);
    return unit;
  }

  removeUnit(unit) {
    let player = this.getPlayer(unit.playerId);
    let idx = player.units.indexOf(unit);
    player.units.splice(idx, 1);
    for (let mi = this.movementOrder.length - 1; mi >= 0; mi--) {
      let mv = this.movementOrder[mi];
      if (mv.playerId === unit.playerId) {
        if (mv.unitNr === idx) {
          this.movementOrder.splice(mi, 1);
        } else if (mv.unitNr > idx) {
          mv.unitNr--;
        }
      }
    }
    this.updateMovementOrder();
    unit.destroy();
    if (this.currentStep && this.currentStep.playerId === unit.playerId && this.currentStep.unitNr >= idx) {
      let newStep = {...this.currentStep}
      newStep.unitNr--;
      if (newStep.unitNr < 0) {
        newStep = this.movementOrder.shift();
      }
      this.updateCurrentStep(newStep);
    }
    this.onSwitch.dispatch();
  }

  addBall(ball, hexlike) {
    let cell = this.grid.getHexBy(hexlike.col, hexlike.row);
    if (!cell || !cell.isFreeToStand) throw new Error("Cannot stand here");
    cell.addUnit(ball);
    this.group.add(ball);
    this.balls.push(ball);
    return ball;
  }

  addGates(gates, hexlike) {
    let player = this.getPlayer(gates.playerId);
    player.gates = gates;

    gates.pattern.forEach(hex => {
      let cell = this.grid.getHexBy(hexlike.col + hex.col, hexlike.row + hex.row);
      if (!cell || !cell.isFreeToStand) throw new Error("Cannot stand here");
      if (hex.goal) {
        cell.setGoal(player);
      }
      if (hex.goalpost) {
        let goalpost = new Goalpost();
        cell.addUnit(goalpost);
        cell.flags.solid = true;
        cell.flags.goalpost = true;
        goalpost.locate();
        this.group.add(goalpost);
      }
    });
    let { x, y } = game.gridview.getMiddlePoint(hexlike);
    gates.x = x;
    gates.y = y;
    gates.hex = hexlike;

    this.group.add(gates);
    return gates;
  }

  addSurpriseBox(surprise, hexlike = undefined) {
    if (hexlike === undefined) {
      hexlike = game.rnd.pick(this.grid.hexes.filter(hex => hex.isFreeToStand && !hex.bonus && !hex.flags.fire && !hex.flags.goal && hex.row !== 0));
    }
    this.bonuses.push(surprise);
    this.group.add(surprise);
    let cell = this.grid.getHexBy(hexlike.col, hexlike.row);
    cell.setBonus(surprise);
    cell.flags.passable = true;
    return surprise;
  }

  onBonusGet(someone, bonus) {
    if (someone.addOnetimeAbility) {
      game.sounder.play('bonus');
      someone.addOnetimeAbility(bonus.ability);
      this.onAbilityChanged.dispatch();
    }
    game.sounder.play('flap');
    bonus.hex.setBonus(undefined);
    this.bonuses.splice(this.bonuses.indexOf(bonus), 1);
  }

  addObstacle(obstacle) {
    if (obstacle.pattern.some(ptrn => !this.grid.getHexBy(obstacle.col + ptrn.col, obstacle.row + ptrn.row).isFreeToStand)) return;
    obstacle.pattern.forEach(ptrn => {
      let cell = this.grid.getHexBy(obstacle.col + ptrn.col, obstacle.row + ptrn.row);
      if (!cell || !cell.isFreeToStand) throw new Error("Cannot stand here");
      cell.flags.obstacle = !obstacle.flags.passable;
      cell.setObstacle(obstacle);
      cell.flags = Object.assign(cell.flags, obstacle.flags);
    });
    this.group.add(obstacle);
    if (obstacle.flags.passable) {
      this.group.sendToBack(obstacle);
    }
    return obstacle;
  }

  getPlayer(id) {
    return this.players[id];
  }

  getUnit({ playerId, unitNr }) {
    return this.players[playerId].units[unitNr];
  }

  start() {
    this.movementOrder = [];
    this.updateMovementOrder();
    this.endTurn();
  }

  get currentPlayer() {
    return this.players[this.currentStep.playerId];
  }

  get currentUnit() {
    return this.currentPlayer.units[this.currentStep.unitNr];
  }

  updateCurrentStep(currentStep) {
    if (this.currentStep) {
      let unit = this.getUnit(this.currentStep);
      if (unit) unit.beforeSwitchFrom();
    }
    this.currentStep = currentStep;
    if (currentStep) {
      let unit = this.getUnit(this.currentStep);
      if (unit) unit.beforeSwitchTo();
    }
  }

  endTurn() {
    this.updateCurrentStep(this.movementOrder.shift());
    this.updateMovementOrder();
    this.onSwitch.dispatch();
  }

  prepareMoveUnitTo(unit, hexlike) {
    let newCell = this.grid.getHexBy(hexlike.col, hexlike.row);
    let action = new MoveAction(unit, unit.hex, newCell);
    if (action.canFastKick) {
      let path = this.grid.findPath(action.from, action.to)
      let prevP = unit.hex;
      for (let p of path) {
        if (p.ball) {
          let possibleTargets = this.grid.getSector(p, { from: prevP, to: p }, this.getKickDistance(unit, p.ball), Math.PI / 4, 0)
            .filter(hex => !path.some(hx => hx.col === hex.col && hx.row === hex.row))
            .filter(cell => cell.isFreeToStand)
            .sort((a, b) => {
              const da = this.grid.getDistance(unit.hex, a);
              const db = this.grid.getDistance(unit.hex, b);
              if (da === db) return 0;
              return da < db ? 1 : -1
            })
          possibleTargets = possibleTargets.slice(0, Math.max(1, Math.floor(possibleTargets.length * 0.2)));
          let target = game.rnd.pick(possibleTargets);
          if (target) {
            let kickAction = this.prepareKickBallTo(unit, p.ball, target, true);
            action.addBallKick(kickAction);
          } else {
            //strange. do not let stand on ball
            action.to = prevP;
            break;
          }
        }
        prevP = p;
      }
    }
    return action;
  }

  //logic-only, not animation!!!
  moveUnitTo(unit, hexlike) {
    if (unit.isPrepared(SPRINT.type)) game.sounder.play('swoosh'); //logic only, soooqa! naaah
    let action = this.prepareMoveUnitTo(unit, hexlike);
    this.checkAbilityAction(unit, "move");
    let oldHex = unit.hex;
    unit.hex.removeUnit(unit);
    action.target.addUnit(unit);
    unit.spendActionPoints(unit.walkCost);

    for (let ka of action.ballKicks) {
      ka.ball.hex.removeUnit(ka.ball);
      ka.target.addUnit(ka.ball);
    }

    this.onActionDone.dispatch(action);
  }


  afterTurn() {
    if (this.currentUnit.afterTurn) {
      this.currentUnit.afterTurn();
      this.onAbilityChanged.dispatch();
    }
  }

  beforeEndTurn() {
    this.checkDeads();
    this.iterateEffects();
  }

  checkDeads() {
    let dead = this.group.children.filter(c => !c.alive).slice();
    for (let d of dead) {
      if (d.hex) this.removeUnit(d);
    }
  }

  checkEndTurn(unit) {
    if (!unit.alive || !unit.hasActionPoints) {
      this.endTurn();
    }
  }

  // does not cost action points
  moveToSlot(unit) {
    let newCell = this.grid.getHexBy(unit.slot.col, unit.slot.row);
    let action = new FreeMoveAction(unit, unit.hex, newCell);
    this.prepareAbility(undefined, unit);
    unit.hex.removeUnit(unit);
    action.target.addUnit(unit);
    this.onActionDone.dispatch(action);
  }


  checkAbilityAction(unit, actionType) {
    if (unit.preparedAbility && unit.preparedAbility.action !== undefined && unit.preparedAbility.action !== actionType) {
      this.prepareAbility(undefined, unit);
    }
  }

  prepareAbility(ability, unit = this.currentUnit) {
    this.currentUnit.prepareAbility(ability);
    this.onAbilityChanged.dispatch();
  }

  skipUser(unit) {
    unit.actionPoints = 0;
    this.prepareAbility(undefined, unit);
    this.onActionDone.dispatch(new SkipUnit(unit));
  }

  overwatchUser(unit) {
    this.prepareAbility(OVERWATCH_PSEUDO_ABILITY, unit);
    unit.actionPoints = 0;
    this.onActionDone.dispatch(new SkipUnit(unit).setOverwatch(true));
  }

  relocateUnit(unit, hexlike) {
    unit.hex.removeUnit(unit);
    this.grid.getHexBy(hexlike.col, hexlike.row).addUnit(unit);
  }

  findUnitToGetPass(hexlike, unit) {
    let cell = this.grid.getHexBy(hexlike.col, hexlike.row);
    if (cell.unit && cell.unit !== unit && cell.unit.playerId === unit.playerId && cell.unit.canGetDirectPass) {
      return cell.unit;
    }

    let cellsAround = this.grid.getInRadius(hexlike, 1);
    for (let cell of cellsAround) {
      let dist = this.grid.getDistance(hexlike, cell);
      if (cell.unit && cell.unit !== unit && cell.unit.playerId === unit.playerId && cell.unit.canGetDirectPass && cell.unit.passRadius >= dist) {
        return cell.unit;
      }
    }

    //todo: here you may check any radius and find appropriate unit
    return null;
  }

  prepareKickBallTo(unit, ball, hexlike, exact = false) {

    let unitToGetPass = this.findUnitToGetPass(hexlike, unit);
    let stopOnPrevius = false;
    if (unitToGetPass) {
      exact = true;
      if (this.grid.same(unitToGetPass.hex, hexlike)) {
        //pass directly to unit, stop on prev
        stopOnPrevius = true;
      } else {
        //pass directly to specified point
      }
      //console.log('do pass to', unitToGetPass);
    }

    if (!exact) {
      hexlike = game.rnd.pick(this.grid.getInRadius(hexlike, this.getMissRadius(unit, ball)).filter(g => g.flags.canKick).concat([hexlike]));
    }
    let oldHex = ball.hex;
    let newCell = this.grid.getHexBy(hexlike.col, hexlike.row);

    let path = this.grid.getLine(oldHex, hexlike);
    let action = new KickAction(unit, ball, oldHex, newCell);

    action.setPassTo(unitToGetPass);

    if (unit.isPrepared("fire")) {
      game.sounder.play('fireball');
      action.setBurning(true);
    }
    if (unit.isPrepared("heal")) {
      action.addEffect({type: "healing", unit: unit});
    }
    if (unit.isPrepared("slime")) {
      action.addEffect({type: "slime", cooldown: 16});
    }

    let remainingLength = path.length;
    let prev = oldHex;

    //console.log(path.map(cell=> `${cell.row}, ${cell.col}`))


    for (let cell of path) {
      if (cell.flags.goal) {
        action.setGoalAt(cell);
      }
      let doRicochetOf = null;
      if (cell.hasObstacle && cell.flags.solid) {
        doRicochetOf = cell.obstacle;
      } else if (cell.hasUnit && cell.unit !== unit) {
        let unit = cell.unit;
        if (unit === unitToGetPass) {
          //do nothing
          if (stopOnPrevius) {
            action.to = prev;
            //console.log('do stop on cell', prev.row, prev.col, ' pass receiver at', unitToGetPass.hex.row, unitToGetPass.hex.col)
            break;
          }
        } else if (unit.solid) {
          //ricochet
          doRicochetOf = unit;
        } else {
          if (unit.getHitAction) {
            let hitAction = unit.getHitAction(ball, action);
            action.setUnitHit(unit, unit.hex, hitAction);
            if (hitAction.ricochet) {
              doRicochetOf = unit;
            } else if (hitAction.stop) {
              action.setRicochetAt(unit, prev);
            }
          }
        }
      }
      if (doRicochetOf && !action.ricochet) {
        let ricochetLength = Math.min(2, remainingLength);
        let ricochetTo = game.rnd.pick(this.grid.getSector(cell, { from: cell, to: oldHex }, ricochetLength, Math.PI / 4, Math.max(0, ricochetLength-2))
          .filter(cell => cell.isFreeToStand && !cell.flags.autoricochet));
        if (ricochetTo) {
          action.setRicochetAt(doRicochetOf, ricochetTo);
          break;
        }
      }
      prev = cell;
      remainingLength--;
    }
    if (!action.ricochet && newCell.flags.autoricochet) {
      let ricochetTo = game.rnd.pick(this.grid.getSector(newCell, { from: newCell, to: oldHex }, 2, Math.PI / 4, 1)
        .filter(cell => cell.isFreeToStand && !cell.flags.autoricochet));
      if (ricochetTo) {
        action.setRicochetAt(newCell, ricochetTo);
      }

    }

    return action;
  }

  kickBallTo(unit, ball, hexlike, exact = false) {
    let action = this.prepareKickBallTo(unit, ball, hexlike, exact);
    this.checkAbilityAction(unit, "kick");
    ball.hex.removeUnit(ball);
    action.target.addUnit(ball);
    unit.spendActionPoints(unit.kickCost);

    this.onActionDone.dispatch(action);

  }


  getAbilities(unit) {
    let abs = [...unit.abilities];
    if (unit.actionPoints === unit.baseActionPoints) {
      abs.unshift(OVERWATCH_PSEUDO_ABILITY);
    } else {
      abs.unshift(SKIP_PSEUDO_ABILITY);
    }
    return abs;
  }

  updateMovementOrder() {
    if (this.movementOrder.length < 8) {
      let allUnits = [];
      for (let p of this.players) {
        allUnits.push(...p.units);
      }
      allUnits.sort((a, b) => b.orderSortKey - a.orderSortKey)
      //allUnits = Phaser.ArrayUtils.shuffle(allUnits);
      this.movementOrder.push(...allUnits.map(unit => ({
        playerId: unit.playerId,
        unitNr: this.players[unit.playerId].units.indexOf(unit)
      })));
    }
  }


  getBallsToKick(unit) {
    return this.balls.filter(ball => this.grid.getDistance(ball.hex, unit.hex) <= unit.kickRadius);
  }

  //difference with getBallsToKick - this method also returns balls available for sprint
  getBallsToInteract(unit) {
    if (!unit.isPrepared("sprint")) return this.getBallsToKick(unit);
    return this.getWalkablePoints(unit).filter(c => c.ball).map(c => c.ball)
  }

  canKick(unit, ball) {
    return this.grid.getDistance(unit.hex, ball.hex) <= unit.kickRadius;
  }

  getKickDistance(unit, ball) {
    let dist = unit.kickDistance;
    if (ball.hasEffect("slime")) {
      dist *= 1.5;
    }
    return dist;
  }

  getMissRadius(unit, ball) {
    let radius = unit.missRadius;
    if (ball.hasEffect("slime")) {
      radius *= 2;
    }
    return radius;
  }

  getKickablePoints(unit, ball) {
    if (!unit.canKick) return [];
    if (unit.isPrepared("360")) {
      return this.grid.getInRadius(ball.hex, unit.kickDistance).filter(c => !c.flags.outside);
    }
    return this.grid.getSector(ball.hex, { from: unit.hex, to: ball.hex }, this.getKickDistance(unit, ball), unit.kickAngle);
  }


  getWalkablePoints(unit) {
    if (!unit.canWalk) return [];
    let check = unit.canFastKick ? ((cell) => cell.isFreeToStand || cell.ball) : undefined;
    return this.grid.getAvailable(unit.hex, unit.walkDistance, check);
  }




}
