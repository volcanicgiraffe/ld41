import path from 'ngraph.path'
import createGraph from 'ngraph.graph'

import HexCell from './cell.js';

export const HEX_WIDTH = 60;
export const HEX_MIDDLE_HEIGHT = 30;
export const HEX_TRIANGLE_HEIGHT = 16;

export default class HexGrid {

  constructor(colsCount, rowsCount) {
    this.hexes = [];
    this.cols = colsCount;
    this.rows = rowsCount;
    this.hexgrid = [];
    for (let row = 0; row < rowsCount; row++) {
      let hexrow = [];
      for (let col = 0; col < this.getColsForRow(row); col++) {
        let cell = new HexCell(col, row);
        this.hexes.push(cell);
        hexrow.push(cell);
      }
      this.hexgrid.push(hexrow);
    }

    for (let hex of this.hexes) {
      let neis = [{row: -1, col:0}, {row: +1, col:0}, {row:0, col:-1}, {row:0, col:+1}];
      if (HexGrid.isShortRow(hex.row)) {
        neis.push({row:+1, col:+1}, {col:+1, row:-1});
      } else {
        neis.push({row:+1, col:-1}, {col:-1, row:-1});
      }
      neis = neis.map(({col, row}) => this.getHexBy(hex.col + col, hex.row + row)).filter(cell => cell);
      hex.neighbours = neis;
    }

    this.initPathfinding();
  }


  getHexBy(col, row) {
    let gridrow = this.hexgrid[row];
    return gridrow && gridrow[col];
  }

  getAvailable(hex, radius, availabilityCheck = cell => cell.isFreeToStand) {
    //todo: add pathfinding
    let out = [];
    let cellsToCheck = [this.getHexBy(hex.col, hex.row)];
    let checkedCellIds = new Set();
    checkedCellIds.add(cellsToCheck[0].id)

    for (let r = 0; r <= radius; r++) {
      let cells = cellsToCheck.slice();
      cellsToCheck = [];
      for (let c of cells) {
        out.push(c);
        for (let n of c.sosedi) {
          if (!checkedCellIds.has(n.id)) {
            checkedCellIds.add(n.id);
            if (availabilityCheck(n)) {
              cellsToCheck.push(n);
            }
          }
        }
      }
    }


    return out; //this.getInRadius(hex, radius).filter(availabilityCheck);
  }

  getMiddlePoint(hexlike) {
    let y = (hexlike.row) * (HEX_MIDDLE_HEIGHT + HEX_TRIANGLE_HEIGHT) + HEX_TRIANGLE_HEIGHT + HEX_MIDDLE_HEIGHT*0.5;
    let x = (hexlike.col + (HexGrid.isLongRow(hexlike.row) ? 0.5 : 1)) * HEX_WIDTH;
    return {x, y};
  }

  getSector(origin, {from, to}, distance, angle = Math.PI/3, minDistance = 0) {
    //todo[grishin]: very ugly, but I don't know how to do better...
    let middleFrom = this.getMiddlePoint(from);
    let middleTo = this.getMiddlePoint(to);
    let middleOrigin = this.getMiddlePoint(origin);
    let middleAngle = Math.atan2(middleTo.y - middleFrom.y, middleTo.x - middleFrom.x);
    let angle1 = middleAngle - angle;
    let angle2 = middleAngle + angle;
    return this.getInRadius(origin, distance).filter(hex => {
      if (hex.flags.outside) return false;
      let mp = this.getMiddlePoint(hex);
      let mangle = Math.atan2(mp.y - middleOrigin.y, mp.x - middleOrigin.x);
      if (mangle < angle1) mangle += Math.PI*2;
      if (mangle > angle2) mangle -= Math.PI*2;
      return this.getDistance(origin, hex) > minDistance && mangle > angle1 && mangle <= angle2;
    });
  }

  same(hexlike1, hexlike2) {
    return hexlike1.row === hexlike2.row && hexlike1.col === hexlike2.col;
  }


  getLine(origin, target) {
    let ox = this.getMiddlePoint(origin);
    let tx = this.getMiddlePoint(target);
    return this.getHexesOnLine(ox, tx, origin, target);
  }

  //ox = {x,y}, tx = {x,y}
  getHexesOnLine(ox, tx, origin = null, target = null) {
    let line = new Phaser.Line(ox.x, ox.y, tx.x, tx.y);
    if (!origin) origin = this.slowGetHexAtXY(ox.x, ox.y);
    if (!target) target = this.slowGetHexAtXY(tx.x, tx.y);
    if (!origin || !target) return [];
    let out = [];
    for (let col = Math.min(origin.col, target.col); col <= Math.max(origin.col, target.col); col++) {
      for (let row = Math.min(origin.row, target.row); row <= Math.max(origin.row, target.row); row++) {
        let mx = this.getMiddlePoint({col,row});
        let distance = Math.abs((tx.y-ox.y)*mx.x - (tx.x-ox.x)*mx.y + tx.x*ox.y - tx.y*ox.x) / line.length;
        if (distance < 30) { //todo: use ball radius!
          out.push(this.getHexBy(col, row));
        }
      }
    }
    return out.filter(c => c).sort((c1, c2) => {
      return this.getDistance(c1, origin) - this.getDistance(c2, origin);
    })
  }

  slowGetHexAtXY(x, y) {
    let closest = null;
    for (let hex of this.hexes) {
      let p = this.getMiddlePoint(hex);
      let d = Math.hypot(p.x - x, p.y - y);
      if (!closest || d < closest.d) {
        closest = {hex, d};
      }
    }
    return closest.hex;
  }

  //dx,dy - pixels!
  getHexAtVector(myHex, dx, dy, distance) {
    let ox = this.getMiddlePoint(myHex);
    let vector = new Phaser.Point(dx, dy);
    vector.normalize();
    vector.setMagnitude(distance*60); //todo: bad const
    let tx = {x: ox.x + vector.x, y: ox.y + vector.y};
    let line = this.getHexesOnLine(ox, tx, myHex);
    let out = undefined;
    for (let p of line) {
      if (p.col === myHex.col && p.row === myHex.row) continue;
      if (!p.isFreeToStand) break;
      if (this.getDistance(myHex, p) > distance) break;
      out = p;
    }
    return out || myHex;
  }

  getInRadius(hex, radius) {
    return this.hexes.filter(h => this.getDistance(h, hex) <= radius)
  }

  forEach(cb) {
    this.hexes.forEach(cb);
  }

  getDistance(hex1, hex2) {
    return HexGrid.getDistance(hex1, hex2);
  }

  static getDistance(hexlike1, hexlike2) {
    if (hexlike1.col > hexlike2.col) {
      return HexGrid.getDistance(hexlike2, hexlike1);
    }
    //todo: cache? cache if something more complex
    let drow = Math.abs(hexlike1.row - hexlike2.row);
    let dcol = Math.abs(hexlike1.col - hexlike2.col);
    //assuming that every 2 rows dcol reduced
    let colreduced = Math.ceil(Math.max(0, (drow - (HexGrid.isLongRow(hexlike1.row) ? 1 : 0))) / 2);
    //if (HexGrid.isLongRow(hexlike1.row) && drow > 0) colreduced--;
    dcol = Math.max(0, dcol - colreduced);
    return drow + dcol;
  }

  getColsForRow(row) {
    return HexGrid.isLongRow(row) ? this.cols : this.cols - 1;
  }

  findPath(hexlike1, hexlike2) {
    let cell1 = this.getHexBy(hexlike1.col, hexlike1.row);
    let cell2 = this.getHexBy(hexlike2.col, hexlike2.row);
    return this.npath.find(cell1.id, cell2.id).map(({data}) => this.getHexBy(data.col, data.row)).reverse().slice(1);
  }

  //returns int or null. null == no way at all
  getLogicalDistance(hexlike1, hexlike2, contextUnit = game.logic.currentUnit) {
    let cell = this.getHexBy(hexlike2.col, hexlike2.row);
    if (cell.flags.obstacle) return null;
    let manOnWayWeight = cell.isFreeToStand ? 0 : 999;
    if (cell.ball && contextUnit.canKickOnRun) manOnWayWeight = 0;
    if (cell.bonus) manOnWayWeight = 0;
    return this.getDistance(hexlike1, hexlike2) + (cell.flags.fire && !contextUnit.fireResist ? 100 : 0) + manOnWayWeight;
  }

  initPathfinding() {
    this.ngraph = createGraph();
    let self = this;

    this.npath = path.aStar(this.ngraph, {
      oriented: true,
      distance(fromNode, toNode, link) {
        return self.getLogicalDistance(fromNode.data, toNode.data, game.logic.currentUnit);
      },
      heuristic(fromNode, toNode) {
        return HexGrid.getDistance(fromNode.data, toNode.data);
      }
    });

    this.forEach((hex) => {
      if (hex.flags.obstacle) return;

      this.ngraph.addNode(hex.id, {col: hex.col, row: hex.row});
      hex.neighbours.forEach((neigh) => {
        if (neigh.flags.obstacle) return;
        this.ngraph.addNode(neigh.id, {col: neigh.col, row: neigh.row});
        this.ngraph.addLink(hex.id, neigh.id, {fromId: hex.id, weight: (neigh.flags.fire ? 100 : 0)});
      });
    });
  }

  static isShortRow(row) {
    return row % 2 === 0;
  }

  static isLongRow(row) {
    return row % 2 === 1;
  }

  static test() {
    assertEq(() => HexGrid.getDistance({row:0, col:0}, {row:1, col:1}), 1);
    assertEq(() => HexGrid.getDistance({row:1, col:1}, {row:0, col:0}), 1);
    assertEq(() => HexGrid.getDistance({row:1, col:1}, {row:0, col:2}), 2);
    assertEq(() => HexGrid.getDistance({row:0, col:2}, {row:1, col:1}), 2);

    assertEq(() => HexGrid.getDistance({row:1, col:0}, {row:1, col:1}), 1);
    assertEq(() => HexGrid.getDistance({row:1, col:0}, {row:3, col:1}), 2);
  }

}

HexGrid.test();
function assertEq(fn, b) {
  if (fn() !== b) throw new Error("failed: " + fn.toString() + " is " + fn() + " != " + b);
}
