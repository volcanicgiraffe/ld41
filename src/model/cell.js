export default class HexCell {

  constructor(col, row) {
    this.col = col;
    this.row = row;
    this.effects = [];
    this.flags = {

    };
    this.neighbours = [];
    this.units = [];
    this.obstacle = undefined;
    this.bonus = undefined;

    this.goal = undefined;
  }

  //I'm tired of misspelling that...
  get sosedi() {
    return this.neighbours;
  }

  setGoal(player) {
    this.goal = player;
    this.flags.goal = true;
  }

  setObstacle(obstacle) {
    this.obstacle = obstacle;
  }

  setBonus(bonus) {
    if (this.bonus) this.bonus.hex = undefined;
    this.bonus = bonus;
    if (bonus) bonus.hex = this;
  }

  get hasObstacle() {
    return !!this.obstacle;
  }

  get isFreeToStand() {
    return !this.units.length && !this.flags.obstacle && !this.flags.outside;
  }

  get hasUnit() {
    return this.units.some(u => u.type !== "ball");
  }

  get ball() {
    return this.units.find(u => u.type === "ball");
  }

  get unit() {
    return this.units.find(u => u.type !== "ball");
  }

  addUnit(unit) {
    this.units.push(unit);
    unit.hex = this;
  }

  removeUnit(unit) {
    this.units.splice(this.units.indexOf(unit), 1);
    unit.hex = undefined;
  }

  get id() {
    if (!this._id) this._id = `${this.col}-${this.row}`;
    return this._id;
  }



}
