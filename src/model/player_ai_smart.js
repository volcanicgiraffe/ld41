import HexGrid from "./grid";
import { SPRINT_ABILITY_MOD } from "../objects/units/baseunit";
import { FIRE, HEAL, SPRINT, X2, K360, SLIME, EAGLE_EYE, DISTANT_KICK } from "../objects/abilities";

const WEIGHT_FIRE = 10;
const WEIGHT_BONUS = -4;

const GATE_GUARD_RADIUS = 5;

export default class PlayerAISmart {
  constructor() {
  }

  set player(value) {
    this._player = value;
  }

  // TODO: icxon, works for current unit only, since uses canWalk (view prop)
  performBestAction(unit) {
    game.logic.prepareAbility(undefined, unit);
    this._fillGridWeights(unit);

    if (this._checkBallKickAction(unit)) return;
    if (this._checkFastKick(unit)) return;

    game.logic.moveUnitTo(unit, this._bestWalkableCell(unit));
  }

  _fillGridWeights(unit) {
    let player = game.logic.getPlayer(unit.playerId);
    let playerGatesHex = player.gates.hex;
    let opponentGatesHex = player.opponent.gates.hex;

    let ball = this._closestBallTo(unit.hex);
    if (ball === null) return; // can't be

    playerGatesHex._aiAllyNearby = 0;
    ball._aiAllyNearby = 0;

    let alliesAlive = 0;

    player.units.forEach((ally) => {
      if (ally.id === unit.id || ally.health <= 0) return;
      if (HexGrid.getDistance(ball.hex, ally.hex) === 1) ball._aiAllyNearby += 1;
      if (HexGrid.getDistance(playerGatesHex, ally.hex) <= GATE_GUARD_RADIUS) playerGatesHex._aiAllyNearby += 1;

      alliesAlive += 1;
    });

    game.grid.forEach(hex => {

      // if (alliesAlive > 0 && playerGatesHex._aiAllyNearby === 0) {
      //   hex._aiMoveWeigth = HexGrid.getDistance(hex, playerGatesHex);
      //   playerGatesHex._aiMoveWeigth = 2;
      // }
      if (ball._aiAllyNearby > 1) {
        if (HexGrid.getDistance(hex, ball.hex) < unit.walkDistance) {
          hex._aiMoveWeigth = HexGrid.getDistance(hex, opponentGatesHex);
        } else {
          hex._aiMoveWeigth = 20;
        }
        opponentGatesHex._aiMoveWeigth = 2;
      } else {
        hex._aiMoveWeigth = HexGrid.getDistance(hex, ball.hex);
      }

      // let playerCond = player.gates.hex.col < ball.hex.col ? hex.col >= ball.hex.col : hex.col <= ball.hex.col;
      // if(ball._aiAllyNearby > 1 && HexGrid.getDistance(hex, ball.hex) <= ALLY_RADIUS && playerCond) hex._aiMoveWeigth += 100;

      hex._aiMoveWeigth += HexGrid.getDistance(hex, playerGatesHex) / 1000;
      hex._aiKickWeight = HexGrid.getDistance(hex, opponentGatesHex);

      if (hex.flags.fire) hex._aiMoveWeigth += WEIGHT_FIRE;
      if (hex.bonus !== undefined) {
        let countX2 = unit.getAbilitiesCount(X2.type);
        let countFire = unit.getAbilitiesCount(FIRE.type);
        if (countX2 === 0) hex._aiMoveWeigth += WEIGHT_BONUS;
        if (countFire === 0) hex._aiMoveWeigth += WEIGHT_BONUS;
        // TODO: low hp
      }
    });

    // player.units.forEach((ally)=> {
    //   console.log("###", ally.constructor.name, HexGrid.getDistance(unit.hex, ally.hex), ally.health, ally.id);
    //
    //   if (ally.id === unit.id || ally.health <= 0) return;
    //   if (HexGrid.getDistance(unit.hex, ally.hex) > ALLY_RADIUS)return;
    //
    //   hex._aiMoveWeigth += ALLY_RADIUS / HexGrid.getDistance(unit.hex, ally.hex) * ALLY_WEIGHT;
    // });
  }

  _checkBallKickAction(unit) {
    let ball = this._closestBallTo(unit.hex);
    if (ball === null || HexGrid.getDistance(unit.hex, ball.hex) !== 1) return false;

    let ability = undefined;

    if (!ability) ability = this._tryAbilityX2(unit, ball);
    if (!ability) ability = this._tryAbilityFire(unit, ball);
    if (!ability) ability = this._tryAbilityHeal(unit, ball);
    if (!ability) ability = this._tryAbilityK360(unit, ball);
    if (!ability) ability = this._tryAbilitySlime(unit, ball);
    if (!ability) ability = this._tryAbilityEagleEye(unit, ball);

    if (ability) game.logic.prepareAbility(ability);


    game.logic.getKickablePoints(unit, ball).forEach(hex => {
      hex.flags.canKick = true;
      if (game.logic.findUnitToGetPass(hex, unit)) {
        hex.flags.canPass = true;
      }
    });

    let bestKick = this._bestKickCell(unit);

    if (unit.canKick && bestKick !== null && bestKick.flags.canKick) {
      game.logic.kickBallTo(unit, ball, bestKick);
      return true;
    }
    return false;
  }

  _tryAbilityX2(unit, ball) {
    // todo: icxon, check if need to apply (kickDistance * 1 < distanceToGate < kickDistance * 2)

    let ability = unit.getFirstAvailableAbility(X2.type);

    return ability;
  }

  _tryAbilityFire(unit, ball) {
    let ability = unit.getFirstAvailableAbility(FIRE.type);

    return ability;
  }

  _tryAbilityHeal(unit, ball) {
    let ability = unit.getFirstAvailableAbility(HEAL.type);

    return ability;
  }

  _tryAbilityK360(unit, ball) {
    let ability = unit.getFirstAvailableAbility(K360.type);

    return ability;
  }

  _tryAbilitySlime(unit, ball) {
    let ability = unit.getFirstAvailableAbility(SLIME.type);

    return ability;
  }

  _tryAbilityEagleEye(unit, ball) {
    let ability = unit.getFirstAvailableAbility(EAGLE_EYE.type);

    return ability;
  }

  _checkFastKick(unit) {
    if (Math.random() < 0.6) return false;

    let ability = unit.getFirstAvailableAbility(SPRINT.type);
    if (!ability) return false;

    let ball = this._closestBallTo(unit.hex);

    // don't sprint if ball is to close and we have some spare ap
    if (ball === null || (HexGrid.getDistance(unit.hex, ball.hex) <= unit.baseWalkDistance && unit.actionPoints > unit.walkCost)) return false;

    // don't sprint if ball is not on the same line
    if (Math.abs(ball.hex.col - unit.hex.col) < 5) return false;

    if (HexGrid.getDistance(unit.hex, ball.hex) > unit.baseWalkDistance * SPRINT_ABILITY_MOD) return false;

    game.logic.prepareAbility(ability);
    game.logic.moveUnitTo(unit, ball.hex);

    return true;
  }

  _bestKickCell(unit) {
    let best = null;
    let min = Number.MAX_VALUE;

    game.grid.forEach(hex => {
      if (hex.flags.canKick && hex._aiKickWeight !== undefined && hex._aiKickWeight < min) {
        best = hex;
        min = hex._aiKickWeight;
      }
    });

    return best;
  }

  _bestWalkableCell(unit) {
    let best = this._randomWalkableCell(unit);
    let min = Number.MAX_VALUE;

    game.grid.forEach(hex => {
      if (hex.flags.canWalk && hex.isFreeToStand && hex._aiMoveWeigth !== undefined && hex._aiMoveWeigth < min) {
        best = hex;
        min = hex._aiMoveWeigth;
      }
    });

    if (!best) best = unit.hex;

    return best;
  }

  _randomWalkableCell(unit) {
    let walkable = [];

    game.grid.forEach(hex => {
      if (hex.flags.canWalk && hex.isFreeToStand) walkable.push(hex);
    });

    return game.rnd.pick(walkable);
  }

  _closestBallTo(hex) {
    let closest = null;
    let min = Number.MAX_VALUE;
    game.logic.balls.forEach(ball => {
      if (HexGrid.getDistance(hex, ball.hex) < min) {
        min = HexGrid.getDistance(hex, ball.hex);
        closest = ball;
      }
    });
    return closest;
  }
}
