import {SELECTION_BLUE, SELECTION_GREEN, SELECTION_RED, SELECTION_YELLOW} from "../effects/selection";

const COLORS = [
  SELECTION_BLUE,
  SELECTION_GREEN,
  SELECTION_RED,
  SELECTION_YELLOW
];

export default class Player {
  constructor(name, id, color = COLORS[id % COLORS.length]) {
    this.id = id;
    this.name = name;
    this.units = [];
    this.color = color;
    this.gates = null; // must be set
    this.slots = [];

    this.score = 0;
    this._ai = null;
  }

  set ai(value) {
    this._ai = value;
    this._ai.player = this;
  }

  get ai() {
    return this._ai;
  }

  get isAi() {
    return this._ai !== null;
  }

  get colorStr() {
    return `rgb(${this.color.r}, ${this.color.g}, ${this.color.b})`;
  }

  get colorTint() {
    return (this.color.r << 16) | (this.color.g << 8) | (this.color.b);
  }
}
