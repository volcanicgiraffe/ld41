import HexGrid from "./grid";
import {HEX_WIDTH, HEX_MIDDLE_HEIGHT, HEX_TRIANGLE_HEIGHT} from "./grid";

/*
     width
     |-----|
        .   --
       ...   | triangle_height
      ..... --
      .....  | middle_height
      .....  |
      ..... --
       ...
        .
 */

function prepareHexBitmap(fillColor, strokeColor, strokeWidth) {
  let bitmap = game.add.bitmapData(HEX_WIDTH, HEX_MIDDLE_HEIGHT + HEX_TRIANGLE_HEIGHT*2);
  let ctx = bitmap.ctx;
  drawHexOn(ctx, fillColor, strokeColor, strokeWidth, 0, 0);
  return bitmap;
}

function drawHexOn(ctx, fillColor, strokeColor, strokeWidth, x, y) {
  ctx.fillStyle = fillColor;
  ctx.strokeStyle = strokeColor;
  ctx.lineWidth = strokeWidth;
  ctx.beginPath();
  ctx.moveTo(x + 0, y + HEX_TRIANGLE_HEIGHT);
  ctx.lineTo(x + HEX_WIDTH/2, y + 0);
  ctx.lineTo(x + HEX_WIDTH-1, y + HEX_TRIANGLE_HEIGHT);
  ctx.lineTo(x + HEX_WIDTH-1, y + HEX_TRIANGLE_HEIGHT + HEX_MIDDLE_HEIGHT);
  ctx.lineTo(x + HEX_WIDTH/2, y + HEX_TRIANGLE_HEIGHT + HEX_MIDDLE_HEIGHT + HEX_TRIANGLE_HEIGHT-1);
  ctx.lineTo(x + 0, y + HEX_TRIANGLE_HEIGHT + HEX_MIDDLE_HEIGHT);
  ctx.lineTo(x + 0, y + HEX_TRIANGLE_HEIGHT);
  if (fillColor) ctx.fill();
  if (strokeColor) ctx.stroke();
}

class HexCellView extends Phaser.Sprite {
  constructor(hex, {x, y}, bitmap) {
    super(game, x, y, bitmap);
    this.hex = hex;
    this.anchor.set(0.5, 0.5);

    this.circle = game.add.sprite(0, 0, "field_elements", "circle.png")
    this.circle.anchor.set(0.5, 0.5);
    this.addChild(this.circle);
    this.circle.visible = false;
    this.circle.alpha = 0.8;
  }

  update() {
    this.visible = false;
    this.tint = 0xffffff;
    this.circle.visible = false;
    this.circle.tint = 0xffffff;
    this.circle.scale.set(0.5, 0.5);
    if (this.hex.flags.goal){
      this.tint = 0xFFFF00;
      this.visible = true;
    //} else if (this.hex.flags.goalpost){
    //  this.tint = 0x00FFFF;
    //  this.visible = true;
    } else if (this.hex.flags.obstacle){
      //this.tint = 0xFF00FF;
      //this.visible = true;
    //} else if (this.hex.flags.autoricochet){
      //this.tint = 0xaa4444;
      //this.visible = true;
    } else if (this.hex.flags.highlighted && this.hex.flags.canWalk) {
      if (this.hex.flags.fire || this.hex.ball) {
        this.tint = 0xff0000;
      } else {
        this.tint = 0x00ff00;
      }
      this.visible = true;
    } else if (this.hex.flags.canKickBallHere) {
      this.visible = true;
      this.tint = 0xccffff;
    } else if (this.hex.flags.canWalk) {
      this.visible = true;
      this.tint = 0xcccccc;
    } else if (this.hex.flags.selected) {
      this.tint = 0xffc000;
      this.visible = true;
    } else {
      this.tint = 0xffffff;
    }

    if (this.hex.ball) {
      this.scale.set(0.8, 0.8);
      this.visible = true;
    } else {
      this.scale.set(1, 1);
    }

    if (this.hex.flags.canKick) {
      this.circle.visible = true;
      this.visible = true;
      if (this.hex.flags.canPass) {
        this.circle.tint = 0x00ff88;
      } else if (this.hex.hasUnit || this.hex.flags.goal) {
        this.circle.tint = 0xff0000;
      }
      if (this.hex.flags.highlighted) {
        this.circle.scale.set(1.2,1.2);
      }
    }
  }
}

export default class HexGridView {
  constructor(grid, group) {
    this.grid = grid;
    this.bitmap = prepareHexBitmap("rgba(255,255,255,0.2)", "white", 1);
    this.hexSprites = [];
    this.bigMap = game.add.bitmapData(this.grid.cols * HEX_WIDTH, this.grid.rows * (HEX_MIDDLE_HEIGHT + HEX_TRIANGLE_HEIGHT));
    this.lookupBitmap = game.add.bitmapData(this.grid.cols * HEX_WIDTH, this.grid.rows * (HEX_MIDDLE_HEIGHT + HEX_TRIANGLE_HEIGHT));
    this.grid.forEach(hex => {
      if (hex.flags.outside) return;
      let point = this.getMiddlePoint(hex);
      this.hexSprites.push(group.add(new HexCellView(hex, point, this.bitmap)));
      drawHexOn(this.lookupBitmap.ctx, `rgb(${hex.row},${hex.col},1)`, null, null, point.x - HEX_WIDTH/2, point.y - HEX_MIDDLE_HEIGHT/2 - HEX_TRIANGLE_HEIGHT);
      drawHexOn(this.bigMap.ctx, "rgba(255,255,255,0.2)", "white", 2, point.x - HEX_WIDTH/2, point.y - HEX_MIDDLE_HEIGHT/2 - HEX_TRIANGLE_HEIGHT);
    });
    this.lookupBitmap.update();
    this.lookupPix = {};
    let bgSprite = game.add.sprite(group.x, group.y, this.bigMap);
    bgSprite.alpha = 0.1;
    game.bgGroup.add(bgSprite);

  }

  getMiddlePoint(hexlike) {
    return this.grid.getMiddlePoint(hexlike);
  }


  getHexBy(x, y) {
    if (x < 0 || y < 0 || x >= this.lookupBitmap.width || y >= this.lookupBitmap.height) return undefined;
    this.lookupBitmap.getPixel(x|0, y|0, this.lookupPix);
    return this.lookupPix.b === 1 ? this.grid.getHexBy(this.lookupPix.g, this.lookupPix.r) : undefined;
  }


}
