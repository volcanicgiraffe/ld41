const BORDER_PAD = 10;
const CONTENT_PAD = 20;

export default class TextWindow extends Phaser.Sprite {
  constructor(x, y) {
    super(game, x, y);
    this.prepareTextures();
    this.anchor.set(0, 0); //top-left
    this._border = game.add.sprite(0, 0, TextWindow.borderTexture);
    this._body = game.add.sprite(0, 0, TextWindow.bodyTexture);
    this.addChild(this._border);
    this.addChild(this._body);
    this._content = game.add.group();
    this.addChild(this._content);

    this._body.x = BORDER_PAD;
    this._body.y = BORDER_PAD;
    this._content.x = BORDER_PAD + CONTENT_PAD;
    this._content.y = BORDER_PAD + CONTENT_PAD;
    this.content = [];
  }

  clearContent() {
    this.content.forEach(c => c.destroy());
    this.content = [];
    return this;
  }

  addContent(data, x = undefined, y = undefined) {
    if (x !== undefined) data.x = x;
    if (y !== undefined) data.y = y;
    this.content.push(data);
    this._content.add(data);
    return this;
  }

  relayout() {
    let maxX = 0, maxY = 0;
    for (let c of this.content) {
      if (!c.visible) continue;
      maxX = Math.max(maxX, c.x + c.width);
      maxY = Math.max(maxY, c.y + c.height);
    }
    this._body.width = maxX + CONTENT_PAD*2;
    this._body.height = maxY + CONTENT_PAD*2;
    this._border.width = maxX + CONTENT_PAD*2 + BORDER_PAD*2;
    this._border.height = maxY + CONTENT_PAD*2 + BORDER_PAD*2;

    this.realHeight = maxY + CONTENT_PAD*2 + BORDER_PAD*2;
    return this;
  }

  prepareTextures() {
    if (!TextWindow.borderTexture) {
      TextWindow.borderTexture = game.add.bitmapData(1,1);
      TextWindow.borderTexture.fill(48,21,20)
      TextWindow.bodyTexture = game.add.bitmapData(1,1);
      TextWindow.bodyTexture.fill(140,133,109)
    }
  }
}