import BitmapRect from "./BitmapRect";
import TextWindow from "./TextWindow";

const SPACING = 20;
const PADDING_BOTTOM = 12;

export default class StatePanel extends Phaser.Sprite {

  constructor(x, y) {
    super(game, x, y);
    game.ui.add(this);

    let dx = SPACING;
    let dy = game.canvas.height;

    this.exitBtn = game.add.button(dx, dy, 'ui', () => {
      game.state.start("menu");
    }, this, 'menu_button_hover.png', 'menu_button.png', 'menu_button_hover.png'); // TODO: icxon: sounds
    this.exitBtn.onInputOver.add((e) => e.tint = 0xFFA57F, this);
    this.exitBtn.onInputOut.add((e) => e.tint = 0xFFFFFF, this);
    this.exitBtn.onInputDown.add((e) => e.tint = 0xAF7057, this);
    this.help = game.add.sprite(420, 120, 'help');
    this.help.visible = false;
    this._addToBottom(this.exitBtn);
    dx += this.exitBtn.width + SPACING;

    this.tacticsBtn = game.add.button(dx, dy, 'ui', () => {
      this.help.visible = !this.help.visible
    }, this, 'bighelp_button_hover.png', 'bighelp_button.png', 'bighelp_button_hover.png');
    this._addToBottom(this.tacticsBtn);
    dx += this.tacticsBtn.width + SPACING;

    this.playerPanel = game.add.sprite(dx, dy, 'ui', 'player_base.png');
    this._addToBottom(this.playerPanel);
    dx += this.playerPanel.width + SPACING;

    this._apIcons = [];
    for (let i = 0; i < 10; i++) {
      let apIcon = game.add.sprite(140 + i * 28, 30, 'ui', 'ap_big.png');
      this.playerPanel.addChild(apIcon);
      this._apIcons.push(apIcon);
    }

    this.timerPanel = game.add.sprite(dx, dy, 'ui', 'timer_base.png');
    this._addToBottom(this.timerPanel);
    dx += this.timerPanel.width + SPACING;

    let style = { font: `46px Arial`, fill: "#FFFF99" };
    this.score = game.add.text(this.timerPanel.width / 2, this.timerPanel.height * 0.6, "0 : 0", style);
    this.score.anchor.set(0.5);
    this.timerPanel.addChild(this.score);

    this.orderPanel = game.add.sprite(dx, dy, 'ui', 'order_base.png');
    this._addToBottom(this.orderPanel);
    dx += this.orderPanel.width + SPACING;


    this._portraitGrp = game.add.group();
    this._portraitGrp.x = 310;
    this._portraitGrp.y = 922;
    this.addChild(this._portraitGrp);

    this._shownOrder = [];
    this._nextOrderPortraitsGrp = game.add.group();
    this._nextOrderPortraitsGrp.x = 1145;
    this._nextOrderPortraitsGrp.y = 1001;
    this.addChild(this._nextOrderPortraitsGrp);

    this._abilitiesGrp = game.add.group();
    this._abilitiesGrp.x = 430;
    this._abilitiesGrp.y = 980;
    this._abilities = [];
    this.addChild(this._abilitiesGrp);

    this._hint = new TextWindow();
    game.ui.add(this._hint);

    let caption = game.add.text(0, 0, "Hello", { font: "24pt Arial", fill: "yellow" });
    let text = game.add.text(0, 50, "Hello", { font: "20pt Arial", fill: "white" });
    let warning = game.add.text(20, 100, "Hello", { font: "20pt Arial", fill: "rgb(255,180,0)" });
    this._hint.text = text;
    this._hint.caption = caption;
    this._hint.warning = warning;
    this._hint.addContent(caption).addContent(text).addContent(warning);
    this._hint.alpha = 0;

    game.logic.onSwitch.add(this.onUnitSwitch.bind(this));
    game.logic.onActionDone.add(this.onActionDone.bind(this));
    game.logic.onAbilityChanged.add(this.onAbilityChanged.bind(this));
  }

  onUnitSwitch() {
    this.showPortrait();
    this.showAbilities();
    this.updateOrder();
  }

  onAbilityChanged() {
    this.showAbilities();
  }

  onActionDone() {
    this.showAbilities();
  }

  updateOrder() {
    //todo: animate removal
    let realOrder = game.logic.movementOrder;
    let myOrder = this._shownOrder.slice();
    let mi = 0;
    let ri = 0;
    let showDelete = [];
    this._shownOrder = [];
    let idx = 0;
    for (; ri < realOrder.length && mi < myOrder.length;) {
      let my = myOrder[mi];
      let real = game.logic.getUnit(realOrder[ri]);
      if (my.unit === real) {
        mi++;
        ri++;
        my.index = idx;
        this._shownOrder.push(my);
        idx++;
      } else {
        showDelete.push(my);
        my.index = -1;
        mi++;
      }
    }
    while (this._shownOrder.length < 6 && realOrder[ri]) {
      let my = { unit: game.logic.getUnit(realOrder[ri]), index: idx };
      ri++;
      idx++;
      myOrder.push(my);
      this._shownOrder.push(my);
    }

    for (let toDelete of showDelete) {
      for (let p of toDelete.psprites || []) {
        game.add.tween(p).to({ alpha: 0 }, 100, null, true).onComplete.addOnce(() => p.destroy());
        game.addTurnAnimation(game.add.tween(p.scale).to({ x: 0, y: 0 }, 100, null, true));
      }
    }
    const PORT_STEP = 120;
    for (let my of this._shownOrder) {
      if (!my.psprites) {
        my.psprites = this.addPortraitTo(this._nextOrderPortraitsGrp, my.index * PORT_STEP, 0, my.unit);
        for (let p of my.psprites) {
          p.alpha = 0;
          p.scale.set(0, 0);
          game.add.tween(p).to({ alpha: 1 }, 100, null, true);
          game.addTurnAnimation(game.add.tween(p.scale).to({ x: 1, y: 1 }, 100, null, true));
        }
      } else if (my.psprites[0].x !== my.index * PORT_STEP) {
        for (let p of my.psprites) {
          game.addTurnAnimation(game.add.tween(p).to({ x: my.index * PORT_STEP }, 100, null, true));
        }
      } else {
      }
    }

  }

  showAbilities() {
    this._hint.alpha = 0;
    this._abilities = [];
    this._abilitiesGrp.removeAll(true);

    let freeSlots = [0, 1, 2, 3];

    //console.log('update abilities of', game.logic.currentUnit.constructor.name)

    this._abilities = game.logic.getAbilities(game.logic.currentUnit).map((ability) => {
      if (freeSlots.length === 0) return;
      let isCounted = ability.count !== undefined;
      let slot = isCounted ? freeSlots.pop() : freeSlots.shift();
      let frameNames = isCounted ? {
        normal: "ability_pickup_base.png",
        hover: "ability_pickup_base_hober.png",
        selected: "ability_pickup_selected.png",
      } : {
          normal: "ability_base.png",
          hover: "ability_base_hover.png",
          selected: "ability_base_selected.png",

        };
      if (isCounted && ability.count > 1) {
        //add "stack"
        let stackSprite = game.add.sprite(slot * 80 + 40, 30, "ui", frameNames.normal, this._abilitiesGrp);
        stackSprite.anchor.set(0.5, 0.5);
        stackSprite.x -= 5;
        stackSprite.y -= 5;
      }
      let btnSprite = game.add.sprite(slot * 80 + 40, 30, "ui", frameNames.normal, this._abilitiesGrp);
      btnSprite.anchor.set(0.5, 0.5);
      let abilitySprite = game.add.sprite(0, 0, ...ability.sprite);
      abilitySprite.anchor.set(0.5, 0.5);
      btnSprite.addChild(abilitySprite);
      btnSprite.inputEnabled = true;
      btnSprite.events.onInputDown.add(() => {
        if (disabled()) return;
        if (ability.type === "skip") {
          game.sounder.play('catch');
          game.logic.skipUser(game.logic.currentUnit);
          return;
        } else if (ability.type === "overwatch") {
          game.sounder.play('guard');
          game.logic.overwatchUser(game.logic.currentUnit);
          return;
        }
        game.logic.prepareAbility(ability);
      });
      btnSprite.events.onInputOver.add(() => {
        btnSprite._over = true;
        btnSprite._showHintAfter = 500;
      });
      btnSprite.events.onInputOut.add(() => {
        btnSprite._over = false;
        this._hint.alpha = 0;
      });

      for (let i = 0; i < ability.cost; i++) {
        let apIcon = new BitmapRect(btnSprite.width / 2 - i * 6 - 10, btnSprite.height / 2 + 6, 5, 5); // todo: a333, normal icons for AP cost
        apIcon.tint = 0x00FFFF;
        btnSprite.addChild(apIcon);
      }

      for (let i = 0; i < ability.count; i++) {
        if (i > 5) continue; // genious...
        let countIcon = new BitmapRect(-btnSprite.width / 2 + i * 6 + 10, btnSprite.height / 2 + 6, 5, 5); // todo: a333, normal icons for count cost
        countIcon.tint = 0xFFFF00;
        btnSprite.addChild(countIcon);
      }

      let cooldownInfo = game.add.text(btnSprite.x, btnSprite.y, ``, {
        font: `16px Arial`,
        fill: "#FFFF00",
        fontWeight: 'bold'
      });
      cooldownInfo.anchor.set(0.5);
      this._abilitiesGrp.addChild(cooldownInfo);

      let hasBallsAround = game.logic.getBallsToKick(game.logic.currentUnit).length > 0;

      let ballsRequirementViolated = ability.requireBall && !hasBallsAround;

      function disabled() {
        return ability.disabled || ballsRequirementViolated;
      }

      btnSprite.update = () => {
        btnSprite.inputEnabled = !game.inAnimation && !game.movingToSlots;
        btnSprite.alpha = disabled() ? 0.5 : 1;

        cooldownInfo.text = ability.cooldown ? `CD: ${ability.cooldown}` : ``;

        if (btnSprite._over && !this._hint.alpha) {
          btnSprite._showHintAfter -= game.time.physicsElapsedMS;
          if (btnSprite._showHintAfter <= 0) {
            this._hint.text.text = ability.description;
            this._hint.caption.text = ability.caption;
            if (disabled()) {
              if (ability.cooldown) {
                this._hint.warning.text = "On cooldown, could be used after " + ability.cooldown + " turn" + (ability.cooldown > 1 ? "s" : "");
                this._hint.warning.visible = true;
              } else if (ballsRequirementViolated) {
                this._hint.warning.text = "Stand near ball before use";
                this._hint.warning.visible = true;
              } else if (!game.logic.currentUnit.canAbility(ability)) {
                this._hint.warning.text = "Not enough action points";
                this._hint.warning.visible = true;
              }
            } else {
              this._hint.warning.text = "";
              this._hint.warning.visible = false;
            }
            this._hint.relayout();
            this._hint.x = btnSprite.worldPosition.x - btnSprite.width / 2;
            this._hint.y = btnSprite.worldPosition.y - this._hint.realHeight - btnSprite.height / 2;
            this._hint.alpha = 1;
          }
        }

        if (disabled()) {
          btnSprite.frameName = frameNames.normal;
          return;
        }
        if (ability.deleted) {
          let tween = game.add.tween(btnSprite.scale).to({ x: 0, y: 0 }, 100, null, true);
          game.addTurnAnimation(tween);
          tween.onComplete.addOnce(() => {
            btnSprite.destroy();
            cooldownInfo.destroy();
          });
          btnSprite.update = () => {
          }
        } else if (ability === game.logic.currentUnit.preparedAbility) {
          btnSprite.frameName = frameNames.selected;
          btnSprite.children[0].y = 2;
        } else {
          btnSprite.children[0].y = 0;
          btnSprite.frameName = btnSprite._over ? frameNames.hover : frameNames.normal;
        }

        //todo: show count
      };
      return { ability, sprite: btnSprite };
    });
  }

  showPortrait() {
    this._portraitGrp.removeAll(true);
    this.addPortraitTo(this._portraitGrp, 51, 51);
    let hpbar = game.add.sprite(-5, 108, "ui", "hp_bar.png");
    this._portraitGrp.add(hpbar);
    let hpbarbar = game.add.bitmapData(102, 12);
    let hpbarbarbar = game.add.sprite(6, 4, hpbarbar);
    hpbarbarbar.destroy = function () {
      Phaser.Sprite.prototype.destroy.call(this, true, true);
    }
    hpbar.addChild(hpbarbarbar);
    hpbar.update = function () {
      let h = game.logic.currentUnit.health / 100;
      hpbarbar.clear();
      hpbarbar.fill(0, 0, 0);
      hpbarbar.rect(0, 0, h * hpbarbar.width, hpbarbar.height, (h < 0.2 ? "red" : (h < 0.5 ? "yellow" : "green")))

    }
  }

  addPortraitTo(group, x, y, unit = game.logic.currentUnit) {
    let portraitBg = game.add.sprite(x, y, "ui", "player_portrait_gr.png", group);
    portraitBg.tint = game.logic.getPlayer(unit.playerId).colorTint;
    let portrait = game.add.sprite(x, y, "actors", unit.portraitFrameName, group);
    portraitBg.anchor.set(0.5, 0.5);
    portrait.anchor.set(0.5, 0.5);
    return [portrait, portraitBg];
  }

  update() {
    if (game.logic.players.length > 1) {
      let p1score = game.logic.getPlayer(0).score;
      let p2score = game.logic.getPlayer(1).score;
      this.score.text = `${p1score} : ${p2score}`;
    }
    for (let c of this.children) c.update();

    let unit = game.logic.currentUnit;
    if (unit) {
      for (let i = 0; i < this._apIcons.length; i++) {
        this._apIcons[i].visible = i < unit.baseActionPoints;
        this._apIcons[i].frameName = i < unit.actionPoints ? 'ap_big.png' : 'ap_big_empty.png';
      }
    }
  }

  _addToBottom(target) {
    this.addChild(target);
    target.y = game.canvas.height - target.height - PADDING_BOTTOM;
  }
}
