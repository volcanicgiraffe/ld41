export default class BitmapRect extends Phaser.Sprite {
  constructor(x, y, w, h, color = 'rgb(255,255,255)') {
    let bmd = game.add.bitmapData(w, h);
    bmd.rect(0, 0, w, h, color);
    super(game, x, y, bmd);
    this.anchor.set(0.5);
  }
}
