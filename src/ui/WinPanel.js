import BitmapRect from "./BitmapRect";
import { makeButton } from "./Button";

const W = 480;
const H = 340;

export default class WinPanel extends Phaser.Sprite {

  constructor() {
    super(game, 0, 0);
    game.ui.add(this);

    this._overlay = new BitmapRect(0, 0, game.canvas.width, game.canvas.height, 'rgba(0,0,0, 0.4)');
    this._overlay.anchor.set(0);
    this.addChild(this._overlay);

    let border = 5;
    this._border = new BitmapRect((game.canvas.width - (W + border * 2)) / 2, (game.canvas.height - (H + border * 2)) / 2, W + border * 2, H + border * 2, 'rgb(48,21,20)');
    this._border.anchor.set(0);
    this._border.alpha = 0.7;
    this.addChild(this._border);

    this._panel = new BitmapRect((game.canvas.width - W) / 2, (game.canvas.height - H) / 2, W, H, 'rgb(140,133,109)');
    this._panel.anchor.set(0);
    this.addChild(this._panel);

  }

  fillInfo(level) {
    this._match = game.add.text(W / 2, H * 0.2, `The match is over`, {
      font: `32px Arial`,
      fill: "#494639",
      fontWeight: 'bold'
    });
    this._match.anchor.set(0.5);
    this._panel.addChild(this._match);

    this._score = game.add.text(W / 2, H * 0.35, `${level.p1.score} : ${level.p2.score}`, { font: `36px Arial`, fill: "#000000", fontWeight: 'bold' });
    this._score.anchor.set(0.5);
    this._panel.addChild(this._score);

    let winner = level.p1.score > level.p2.score ? level.p1.name : level.p2.name;

    this._winner = game.add.text(W / 2, H * 0.48, `${winner} wins!`, { font: `24px Arial`, fill: "#000000" });
    this._winner.anchor.set(0.5);
    this._panel.addChild(this._winner);

    this._restart = makeButton(W / 2, H * 0.8, 'Exit', () => {
      game.state.start("menu")
    });
    this._panel.addChild(this._restart);

  }
}
