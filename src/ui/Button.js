export function makeButton(x, y, text, action) {
  let button = game.add.button(x, y, 'ui', action, this, 'long_button_pressed.png', 'long_button.png', 'long_button_selected.png');
  button.anchor.set(0.5);

  let label = game.add.text(0, 3, text, { font: `18px Arial`, fill: "#661418", fontWeight: 'bold' });
  label.anchor.set(0.5);
  button.addChild(label);
  label.scale.set(1);
  return button;
}


export function makePortraitButton(x, y, tint, action) {
  let button = game.add.button(x, y, 'ui', action, this, 'player_portrait_gr.png', 'player_portrait_gr.png', 'player_portrait_gr.png');
  button.anchor.set(0.5, 1);
  button.tint = tint;
  return button;
}

export function makeBigPortraitButton(x, y, tint, action) {
  let button = game.add.button(x, y, 'ui', action, this, 'player_portrait_b.png', 'player_portrait_b.png', 'player_portrait_b.png');
  button.anchor.set(0.5, 1);
  button.tint = tint;
  return button;
}
